package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Car;
import model.Engine;

/**
 *
 * @author MMD
 */
@Path("/cars")
@Consumes(MediaType.TEXT_PLAIN)
public class CarResource {

    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String hello() {
        return "Hello!";
    }

    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getCar(@PathParam("id") Long id) {

        return Response.ok(new Car(id, "AUDI", new Engine(200))).build();

    }

//    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getCars(@QueryParam("brand") String brand) {

        List<Car> cars = new ArrayList<>();

        cars.add(new Car(1L, "AUDI", new Engine(200)));
        cars.add(new Car(2L, "PORSCHE", new Engine(500)));

        List<Car> filteredCars = (cars.stream()
                .filter(car -> car.getBrand().equalsIgnoreCase(brand))
                .collect(Collectors.toList()));

//        return Response.ok(cars).build();
        return Response.ok(filteredCars).build();

    }
    
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response createCar(Car car){
        
        return Response.ok(car).build();
        
    }

}
