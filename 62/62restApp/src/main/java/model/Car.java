package model;

import java.io.Serializable;

/**
 *
 * @author MMD
 */
public class Car implements Serializable{

    private String brand;
    private Long id;
    private Engine engine;

    public Car(Long id, String brand, Engine engine) {
        this.id = id;
        this.brand = brand;
        this.engine = engine;
    }

    public Car() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

}
