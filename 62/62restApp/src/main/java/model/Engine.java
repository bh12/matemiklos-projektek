package model;

import java.io.Serializable;

/**
 *
 * @author MMD
 */
public class Engine implements Serializable{

    private int horsepower;

    public Engine(int horsepower) {
        this.horsepower = horsepower;
    }
    
    public Engine() {
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

}
