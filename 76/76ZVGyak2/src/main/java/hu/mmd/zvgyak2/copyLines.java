package hu.mmd.zvgyak2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 *
 * @author MMD
 */
public class copyLines {

    public static void copyEverySecondLine(File f) {

        String resultString = null;

        try (FileReader fr = new FileReader(f); BufferedReader br = new BufferedReader(fr)) {

            String str;
            StringBuilder sb = new StringBuilder();
            int counter = 1;
            str = br.readLine();
            
            while (str != null) {

                if (counter % 2 == 0) {
                    sb.append(str);
                }

                str = br.readLine();
                counter++;
            }

            resultString = sb.toString();
            System.out.println(resultString);

        } catch (Exception e) {
            System.out.println("Hiba a beolvasásnál");
        }

        File newFile = new File("newText.txt");

        try (FileWriter fw = new FileWriter(newFile); BufferedWriter bw = new BufferedWriter(fw)) {

            bw.write(resultString);

        } catch (Exception e) {
            System.out.println("Hiba az íráskor");
        }

    }
}
