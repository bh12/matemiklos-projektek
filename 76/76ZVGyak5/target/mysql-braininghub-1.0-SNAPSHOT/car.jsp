<%-- 
    Document   : car
    Created on : Jun 16, 2020, 5:39:21 PM
    Author     : kopacsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Car</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <form action="CarServlet" method="post">
            
            <label for="carId">Car ID:</label><br>
            <input type="text" id="carId" name="carId"><br><br>
            
            <label for="plateNumber">plateNumber</label><br>
            <input type="text" id="plateNumber" name="plateNumber"><br><br>
            
            <label for="Type">Type</label><br>
            <input type="text" id="Type" name="Type"><br><br>
            
            <label for="km">km</label><br>
            <input type="text" id="km" name="km"><br><br>
            
            <input type="submit" value="Submit">
        </form> 
    </body>
</html>
