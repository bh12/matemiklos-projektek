package Servlet;

import DAO.CarDAO;
import DTO.CarDTO;
import Service.CarService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {
    
    @EJB
    CarDAO carDAO;
    
    @Inject
    CarService carService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("car.jsp").forward(req, res);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Long carId = Long.valueOf(req.getParameter("carId"));
        String plateNumber = req.getParameter("plateNumber");
        String type = req.getParameter("Type");
        int km = Integer.parseInt(req.getParameter("km"));
        
        CarDTO carDTO = new CarDTO();
        
        carDTO.setId(carId);
        carDTO.setPlateNumber(plateNumber);
        carDTO.setType(type);
        carDTO.setKm(km);
        
        carService.addCar(carDTO);
        
        res.sendRedirect("car.jsp");
    }
}
