package Mapper;

import DTO.CarDTO;
import Entity.CarEntity;

/**
 *
 * @author MMD
 */
public class CarMapper {
    public static CarEntity DTOToEntity(CarDTO carDto){
        
        CarEntity ce = new CarEntity();
        
        ce.setId(carDto.getId());
        ce.setPlateNumber(carDto.getPlateNumber());
        ce.setType(carDto.getType());
        ce.setKm(carDto.getKm());
        
        return ce;
    }
}
