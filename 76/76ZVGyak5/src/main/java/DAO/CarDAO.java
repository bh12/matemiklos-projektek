package DAO;

import Entity.CarEntity;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@TransactionAttribute
public class CarDAO {

    @PersistenceContext
    private EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public void addNewCar(CarEntity ce) {
        try {
            em.getTransaction().begin();
            em.persist(ce);

            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
