package Service;

import DAO.CarDAO;
import DTO.CarDTO;
import Entity.CarEntity;
import Mapper.CarMapper;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author MMD
 */
@Stateless
public class CarService {

    @Inject
    CarDAO dao;

    public void addCar(CarDTO carDTO) {
        CarEntity ce = CarMapper.DTOToEntity(carDTO);

        dao.addNewCar(ce);
    }
}
