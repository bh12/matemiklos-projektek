package hu.mmd.gyak1;

/**
 *
 * @author MMD
 */
public abstract class calculator {
    
    public int plus(int a, int b){
        int result = a + b;
        return result;
    }
    public int minus(int a, int b){
        int result = a - b;
        return result;
    }
    public int multiply(int a, int b){
        int result = a * b;
        return result;
    }
    public int divide(int a, int b){
        int result = a / b;
        return result;
    }
}
