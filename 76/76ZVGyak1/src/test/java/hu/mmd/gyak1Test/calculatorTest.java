package hu.mmd.gyak1Test;

import hu.mmd.gyak1.scientificCalculator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author MMD
 */
public class calculatorTest {

    private static scientificCalculator calculator;

    public calculatorTest() {

    }

    @BeforeAll
    public static void setUpClass() {
        calculator = new scientificCalculator();

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void onePlusTwo() {
        int result = calculator.plus(1, 2);

        Assertions.assertEquals(3, result);
    }
}
