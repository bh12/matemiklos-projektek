package hu.mmd.zvgyak4;

/**
 *
 * @author MMD
 */
public class customParsingMethod {

    public static int parsingAndPlus(String number1, String number2) throws numberException {

        int result = 0;

        try {
            int int1 = Integer.parseInt(number1);
            int int2 = Integer.parseInt(number2);
            result = int1 + int2;
        } catch (Exception e) {
            throw new numberException();
        }

        return result;
    }
}
