SELECT * FROM kmmproject.activatefilter_filter_jt;

USE kmmproject;

SELECT Product.* FROM Product 
INNER JOIN activatefilter_product_jt ON Product.id = activatefilter_product_jt.product_id
INNER JOIN activatefilters ON activatefilter_product_jt.activatefilter_id = activatefilters.id
INNER JOIN activatefilter_filter_jt ON activatefilter_filter_jt.activatefilter_id = activatefilters.id
INNER JOIN filter ON activatefilter_filter_jt.filter_id = filter.id
INNER JOIN filter_product_jt ON filter.id = filter_product_jt.filter_id
WHERE activatefilter_product_jt.product_id = 2;

USE kmmproject;

SELECT Product.* FROM Product 
INNER JOIN activatefilter_product_jt ON Product.id = activatefilter_product_jt.product_id
INNER JOIN activatefilters ON activatefilter_product_jt.activatefilter_id = activatefilters.id
INNER JOIN activatefilter_filter_jt ON activatefilter_filter_jt.activatefilter_id = activatefilters.id
INNER JOIN filter ON activatefilter_filter_jt.filter_id = filter.id
INNER JOIN filter_product_jt ON filter.id = filter_product_jt.filter_id
WHERE filter_product_jt.product_id = 3;