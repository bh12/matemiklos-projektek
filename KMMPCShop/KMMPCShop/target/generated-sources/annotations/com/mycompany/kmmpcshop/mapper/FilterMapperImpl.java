package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.FilterDTO;
import com.mycompany.kmmpcshop.entity.FilterEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-16T08:53:08+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class FilterMapperImpl implements FilterMapper {

    @Override
    public FilterDTO toDTO(FilterEntity FilterEntity) {
        if ( FilterEntity == null ) {
            return null;
        }

        FilterDTO filterDTO = new FilterDTO();

        return filterDTO;
    }

    @Override
    public List<FilterDTO> toDTOList(List<FilterEntity> FilterEntities) {
        if ( FilterEntities == null ) {
            return null;
        }

        List<FilterDTO> list = new ArrayList<FilterDTO>( FilterEntities.size() );
        for ( FilterEntity filterEntity : FilterEntities ) {
            list.add( toDTO( filterEntity ) );
        }

        return list;
    }
}
