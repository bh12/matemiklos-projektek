package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.ProductTypeDTO;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-16T08:53:08+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class ProductTypeMapperInterfaceImpl implements ProductTypeMapperInterface {

    @Override
    public ProductTypeEntity toEntity(ProductTypeDTO productTypeDTO) {
        if ( productTypeDTO == null ) {
            return null;
        }

        ProductTypeEntity productTypeEntity = new ProductTypeEntity();

        productTypeEntity.setId( productTypeDTO.getId() );
        productTypeEntity.setName( productTypeDTO.getName() );

        return productTypeEntity;
    }
}
