package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.UserDTO;
import com.mycompany.kmmpcshop.entity.UserEntity;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-16T08:53:08+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDTO toDTO(UserEntity userEntity) {
        if ( userEntity == null ) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setRole( userEntity.getUserRole() );
        userDTO.setEmail( userEntity.getEmail() );
        userDTO.setPassword( userEntity.getPassword() );
        userDTO.setPhone( userEntity.getPhone() );
        userDTO.setAddress( userEntity.getAddress() );
        userDTO.setName( userEntity.getName() );
        userDTO.setDob( userEntity.getDob() );

        return userDTO;
    }

    @Override
    public UserEntity toEntity(UserDTO userDTO) {
        if ( userDTO == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setUserRole( userDTO.getRole() );
        userEntity.setEmail( userDTO.getEmail() );
        userEntity.setPassword( userDTO.getPassword() );
        userEntity.setAddress( userDTO.getAddress() );
        userEntity.setDob( userDTO.getDob() );
        userEntity.setPhone( userDTO.getPhone() );
        userEntity.setName( userDTO.getName() );

        return userEntity;
    }
}
