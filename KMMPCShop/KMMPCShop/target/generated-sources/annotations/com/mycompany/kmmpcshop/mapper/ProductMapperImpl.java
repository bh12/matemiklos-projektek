package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.ProductDTO;
import com.mycompany.kmmpcshop.entity.ProductEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-16T08:53:08+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDTO toDTO(ProductEntity productEntity) {
        if ( productEntity == null ) {
            return null;
        }

        ProductDTO productDTO = new ProductDTO();

        productDTO.setId( productEntity.getId() );
        productDTO.setName( productEntity.getName() );
        productDTO.setManufacturer( productEntity.getManufacturer() );
        productDTO.setDescription( productEntity.getDescription() );
        productDTO.setPrice( productEntity.getPrice() );

        return productDTO;
    }

    @Override
    public List<ProductDTO> toDTOList(List<ProductEntity> productEntities) {
        if ( productEntities == null ) {
            return null;
        }

        List<ProductDTO> list = new ArrayList<ProductDTO>( productEntities.size() );
        for ( ProductEntity productEntity : productEntities ) {
            list.add( toDTO( productEntity ) );
        }

        return list;
    }

    @Override
    public ProductEntity toEntity(ProductDTO productDTO) {
        if ( productDTO == null ) {
            return null;
        }

        ProductEntity productEntity = new ProductEntity();

        productEntity.setId( productDTO.getId() );
        productEntity.setDescription( productDTO.getDescription() );
        productEntity.setName( productDTO.getName() );
        productEntity.setManufacturer( productDTO.getManufacturer() );
        productEntity.setPrice( productDTO.getPrice() );

        return productEntity;
    }
}
