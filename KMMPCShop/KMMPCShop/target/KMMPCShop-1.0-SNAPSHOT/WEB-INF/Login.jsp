<%-- 
    Document   : Login
    Created on : 2020.08.31., 13:34:34
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <%@include file="WEB-INF/HeaderImports.jsp" %>
    </head>
    <body>
        <form method="POST" action="Login">
            <div class="form-group">
                <label for="email">Email:</label>

                <input type="email" class="form-control" placeholder="Email" id="email" name="email" required="">

            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </body>
</html>
