<%-- 
    Document   : newjsp
    Created on : 2020.09.11., 17:03:43
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Forgotten password</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/navbar.jsp" %>
         <form method="POST" action="ForgottenPassword">
                <div class="form-group row">
                    <label for="email">Your email:</label>
                    <input type="email" class="form-control" name="email " id="email">
                </div>

                <button type="submit" class="btn btn-primary">Send new password</button>
         </form>
    </body>
</html>
