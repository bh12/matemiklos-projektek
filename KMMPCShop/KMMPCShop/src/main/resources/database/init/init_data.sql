
INSERT INTO `kmmproject`.`productType` (`id`, `name`) VALUES ('1', 'CPU');
INSERT INTO `kmmproject`.`productType` (`id`, `name`) VALUES ('2', 'VGA');
INSERT INTO `kmmproject`.`productType` (`id`, `name`) VALUES ('3', 'Motherboard');

INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('1', 'Gamer', 'ASUS', 'ROG STRIX B360-M Gaming', '3', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('2', 'Gamer', 'MSI', 'X470 GAMING PRO CARBON AC', '3', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('3', 'WorkStation', 'ASUS', 'WS C621E SAGE', '3', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('4', 'Gamer', 'ASUS', 'GeForce RTX 2080 Ti 11GB GDDR6 OC PCIE', '2', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('5', 'Mining', 'EVGA', 'P104-100 Mining Edition, 04G-P4-5183-RB Graphics Card', '2', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('6', 'Gamer', 'GIGABITE', 'GTX1660 OC 6GB GDDR5 ', '2', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('7', 'Gamer', 'INTEL', 'Core i7-9700K', '1', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('8', 'Gamer', 'INTEL', 'Core i9-10900K', '1', 100);
INSERT INTO `kmmproject`.`product` (`id`, `description`, `manufacturer`, `name`, `productType_id`, price) VALUES ('9', 'WorkStation', 'AMD', 'Ryzen ThreadRipper 3960X', '1', 100);


INSERT INTO `kmmproject`.`filter` (`id`, `parameterName`, `parameterValue`) VALUES ('1', 'SocketType', 'CS3');
INSERT INTO `kmmproject`.`filter` (`id`, `parameterName`, `parameterValue`) VALUES ('2', 'SocketType', 'B2');

INSERT INTO `kmmproject`.`activatefilters` (`id`) VALUES ('1');
INSERT INTO `kmmproject`.`activatefilters` (`id`) VALUES ('2');

INSERT INTO `kmmproject`.`filter_product_jt` (`filter_id`, `product_id`) VALUES ('1', '1');
INSERT INTO `kmmproject`.`filter_product_jt` (`filter_id`, `product_id`) VALUES ('2', '2');
INSERT INTO `kmmproject`.`filter_product_jt` (`filter_id`, `product_id`) VALUES ('2', '3');

INSERT INTO `kmmproject`.`activatefilter_product_jt` (`activatefilter_id`, `product_id`) VALUES ('1', '1');
INSERT INTO `kmmproject`.`activatefilter_product_jt` (`activatefilter_id`, `product_id`) VALUES ('2', '2');

INSERT INTO `kmmproject`.`activatefilter_filter_jt` (`activatefilter_id`, `filter_id`) VALUES ('1', '2');
INSERT INTO `kmmproject`.`activatefilter_filter_jt` (`activatefilter_id`, `filter_id`) VALUES ('2', '1');


