<%-- 
    Document   : UserSettings
    Created on : 2020.09.10., 14:22:00
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>  
    <script>
            $(document).ready(function () {
                var date_input = $('input[name="date"]');
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy/mm/dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_input.datepicker(options);
            });
        </script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Settings</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/navbar.jsp" %>
        <div class="container">
         <form method="POST" action="UpdateUserServlet">
                <div class="form-group row">
                    <label for="phone">Phone:</label>
                    <input type="text" class="form-control" placeholder="${user.phone}" name="phone" id="phone">
                </div>
                <div class="form-group row">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" placeholder="${user.name}" name="name" id="name">
                </div>
                <div class="form-group row">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" placeholder="${user.address}" name="address" id="address">
                </div>
                <div class="form-group row"> 
                    <label class="control-label" for="date">Date of birth:</label>
                    <input class="form-control" id="date" name="date" placeholder="${user.dob}" type="text"/>
                </div>

                <button type="submit" class="btn btn-primary">Save changes</button>
         </form>
        </div>
    </body>
</html>
