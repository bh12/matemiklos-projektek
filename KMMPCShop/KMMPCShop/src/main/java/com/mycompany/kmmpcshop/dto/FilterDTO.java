
package com.mycompany.kmmpcshop.dto;

// There will be the datas of the other tables to filter
public class FilterDTO {
    private String name;
    private String manufacturer;
    private String description;
   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


  
}

