/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.dao;

import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author MMD
 */
@Singleton
public class ProductTypeDAO {

    public static final String QUERY_FIND_ALL = "ProductTypeEntity.findAll";

    private static final String QUERY_FIND_BY_ID
            = "SELECT a FROM ProductTypeEntity a "
            + "WHERE a.id IN ";

    @PersistenceContext
    private EntityManager em;

    public List<ProductTypeEntity> findAll() {
        TypedQuery<ProductTypeEntity> namedQuery = em.createNamedQuery(QUERY_FIND_ALL, ProductTypeEntity.class);
        return namedQuery.getResultList();
    }

    public List<ProductTypeEntity> findById(Long id) {
        
        String srt = String.valueOf(id);
        
        TypedQuery<ProductTypeEntity> query = em.createQuery(QUERY_FIND_BY_ID + srt, ProductTypeEntity.class);
        return query.getResultList();
    }

    public void addNewProd(ProductTypeEntity productType) throws Exception {
        try {
            em.getTransaction().begin();
            em.find(ProductTypeEntity.class, productType.getId());

            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
