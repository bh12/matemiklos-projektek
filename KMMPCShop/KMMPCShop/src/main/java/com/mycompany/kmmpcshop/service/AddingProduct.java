/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.service;

import com.mycompany.kmmpcshop.dao.ProductDAO;
import com.mycompany.kmmpcshop.dao.ProductTypeDAO;
import com.mycompany.kmmpcshop.dto.ProductDTO;
import com.mycompany.kmmpcshop.dto.ProductTypeDTO;
import com.mycompany.kmmpcshop.entity.ProductEntity;

import com.mycompany.kmmpcshop.mapper.ProductMapper;
import com.mycompany.kmmpcshop.mapper.ProductTypeMapper;
import com.mycompany.kmmpcshop.mapper.ProductTypeMapperInterface;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class AddingProduct {

    @Inject
    ProductDAO dao;
    ProductTypeDAO ptDao;
    ProductDTO dto;
    ProductTypeDTO ptdto;

    public void addNewProduct(String description, String manufacturer, String name, int price, long ptId) {

        List<ProductTypeDTO> productTypes = ProductTypeMapper.toDTOList(ptDao.findById(ptId));

        dto = new ProductDTO();
        ptdto = productTypes.get(0);
        dto.setDescription(description);
        dto.setManufacturer(manufacturer);
        dto.setName(name);
        dto.setPrice(price);
        dto.setProductType(ptdto);

        try {
//           ptDao.addNewProd(ProductTypeMapperInterface.INSTANCE.toEntity(ptdto));
//           dto.setProductType();
            dao.addNewProd(ProductMapper.INSTANCE.toEntity(dto));

        } catch (Exception ex) {
            Logger.getLogger(AddingProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
