/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.UserDTO;
import com.mycompany.kmmpcshop.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    
    @Mapping(source="userRole", target="role")
    UserDTO toDTO(UserEntity userEntity);
    @Mapping(source="role",target="userRole")
    UserEntity toEntity(UserDTO userDTO);
}
