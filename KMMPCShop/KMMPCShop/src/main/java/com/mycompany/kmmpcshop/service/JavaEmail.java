
package com.mycompany.kmmpcshop.service;
 
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

 
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 

    //Utility class for sending e-mail messages

public class JavaEmail {
  
    public static String address;
    public static String ourMessage;
    
    public static void sendEmail(String host, String port,
            final String userName, final String password,String toAddress,
            String subject, String message, boolean email) throws AddressException,
            MessagingException,
            UnsupportedEncodingException {
   
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
 
        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };
 
        Session session = Session.getInstance(properties, auth);
        Message msg = new MimeMessage(session);
   // Sets the recipient mail address and the message template, this code 
   //helps us to use this service class when we want to send or receive e-mail
        if(email == true){
            address = userName;
            ourMessage = "\n E-mail sender: "+ toAddress;
            }
        else{
            address = toAddress;
            ourMessage = "\n Thanks for choosing us! Have a nice day";
        }
        // creates a new e-mail message
       
        msg.setFrom(new InternetAddress(address));
        
        InternetAddress[] toAddresses = { new InternetAddress(address) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        msg.setText(message+ourMessage);
        // sends the e-mail
        Transport.send(msg);
 
    }
    
}