package com.mycompany.kmmpcshop.entity;

import static com.mycompany.kmmpcshop.entity.FilterEntity.QUERY_FIND_ALL_FILTER;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Filter")
@NamedQuery(name = QUERY_FIND_ALL_FILTER, query = "select b from FilterEntity b")
public class FilterEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL_FILTER = "FilterEntity.findAll";

    @Column
    private String parameterName;

    @Column
    private String parameterValue;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "filter_product_jt",
            joinColumns = @JoinColumn(name = "filter_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<ProductEntity> products;

    @ManyToMany(mappedBy = "filters")
    private List<ActivateFilterEntity> activatefilters;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(List<ProductEntity> products) {
        this.products = products;
    }

}
