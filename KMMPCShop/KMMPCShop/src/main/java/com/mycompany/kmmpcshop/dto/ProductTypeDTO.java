package com.mycompany.kmmpcshop.dto;

import java.util.List;

/**
 *
 * @author MMD
 */
public class ProductTypeDTO extends BaseDTO {

    private String name;
    private List<ProductDTO> products;
    private FilterDTO filter;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public FilterDTO getFilter() {
        return filter;
    }

    public void setFilter(FilterDTO filter) {
        this.filter = filter;
    }

}
