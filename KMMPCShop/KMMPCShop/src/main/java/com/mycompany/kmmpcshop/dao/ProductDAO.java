package com.mycompany.kmmpcshop.dao;

import com.mycompany.kmmpcshop.entity.ActivateFilterEntity;
import com.mycompany.kmmpcshop.entity.FilterEntity;
import com.mycompany.kmmpcshop.entity.ProductEntity;
import java.util.ArrayList;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Singleton
@TransactionAttribute
public class ProductDAO {

    private static final String QUERY_FIND_PRODUCTS
            = "SELECT a FROM ProductEntity a "
            + "WHERE a.id IN ";

    @PersistenceContext
    private EntityManager em;

    public List<ProductEntity> findAll() {
        TypedQuery<ProductEntity> namedQuery = em.createNamedQuery(ProductEntity.QUERY_FIND_ALL, ProductEntity.class);
        return namedQuery.getResultList();
    }

    public List<ProductEntity> findFiltered(List<Long> filterIds) {

        StringBuilder str = new StringBuilder();

        if (filterIds.isEmpty()) {
            TypedQuery<ProductEntity> namedQuery = em.createQuery(QUERY_FIND_PRODUCTS + "(0)", ProductEntity.class);

            return namedQuery.getResultList();
        } else {

            str.append("(");
            str.append(filterIds.get(0));

            for (int i = 1; i < filterIds.size(); i++) {
                str.append(", ");
                str.append(filterIds.get(i));

            }

            str.append(")");

            TypedQuery<ProductEntity> namedQuery = em.createQuery(QUERY_FIND_PRODUCTS + str.toString(), ProductEntity.class);

            List<ProductEntity> productListStart = namedQuery.getResultList();

            List<ActivateFilterEntity> ActivateFilters = new ArrayList<>();
            List<FilterEntity> Filters = new ArrayList<>();
            
            List<ProductEntity> productListResult = new ArrayList<>();

            for (int i = 0; i < productListStart.size(); i++) {
                ActivateFilters.addAll(productListStart.get(i).getActivateFilters());
            }

            for (int i = 0; i < ActivateFilters.size(); i++) {
                Filters.addAll(ActivateFilters.get(i).getFilters());
            }
            
            for (int i = 0; i < Filters.size(); i++) {
                productListResult.addAll(Filters.get(i).getProducts());
            }
            
//            List<ProductEntity> tmp = productListStart.get(0).getActivateFilters().get(0).getFilters().get(0).getProducts();
            return productListResult;

        }

    }
	 public void addNewProd(ProductEntity product)throws Exception{
       try{
           em.getTransaction().begin();
           em.persist(product);
           
           em.getTransaction().commit();
           
       }
       catch(Exception e){
           e.printStackTrace();
       }
   }

}
