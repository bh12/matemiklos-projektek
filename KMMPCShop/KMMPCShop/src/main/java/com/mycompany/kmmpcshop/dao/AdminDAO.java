package com.mycompany.kmmpcshop.dao;

import com.mycompany.kmmpcshop.entity.ProductEntity;
import com.mycompany.kmmpcshop.entity.UserEntity;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class AdminDAO {
    @PersistenceContext
    private EntityManager em;
    
    public void removeUser(String email){
//        em.remove(user);
    }
    public void addProduct(){
//        em.persist(product);
    }
    public void updateProduct(){         //...
        //...
        //em.persist(product);
    }
    public void removeProduct(){
//        em.remove(product);
    }
    public void removeConfig(){
//        em.remove(config);
    }
    public void addFilter(){
//        em.persist(em);
    }
    public void updateFilter(){       //.....
        //FilterEntity filter=em.find(FilterEntity.class, id);
        //.....
        //em.persist(filter);
    }
    public void removeFilter(){
//        em.remove(filter);
    }
    public UserEntity findUser(String email){
        return null;
    }
    public ProductEntity findProduct(int id){
        ProductEntity product=em.find(ProductEntity.class, id);
        return product;
    }
}
