/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.dto;

import com.mycompany.kmmpcshop.Enum.UserEnum;
import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UserDTO {
    private String email;
    private String password;
    private UserEnum role;
    private String phone;
    private String address;
    private String name;
    private LocalDate dob;

}

