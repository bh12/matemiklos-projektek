/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.servlet;

import com.mycompany.kmmpcshop.bean.ListProductsPageBean;
import com.mycompany.kmmpcshop.dto.ProductDTO;
import com.mycompany.kmmpcshop.entity.ProductEntity;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import com.mycompany.kmmpcshop.mapper.ProductMapper;
import com.mycompany.kmmpcshop.mapper.ProductTypeMapper;
import com.mycompany.kmmpcshop.service.ProductService;
import com.mycompany.kmmpcshop.service.ProductTypeService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author MMD
 */
@WebServlet(name = "ConfProductsServlet", urlPatterns = {"/ConfProducts"})
public class ConfProductsServlet extends HttpServlet {

    private static final String LIST_PRODUCTS_PAGE_BEAN = "listProductsPageBean";

    @Inject
    private ProductService productService;

    @Inject
    private ProductTypeService productTypeService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ListProductsPageBean listProductsPageBean = (ListProductsPageBean) req.getSession()
                .getAttribute(LIST_PRODUCTS_PAGE_BEAN);

        if (listProductsPageBean == null) {
            listProductsPageBean = new ListProductsPageBean();
            req.getSession().setAttribute(LIST_PRODUCTS_PAGE_BEAN, listProductsPageBean);
        }

        List<ProductEntity> filteredProducts = productService.findFiltered(listProductsPageBean.getProductIdsForFilters());
        List<ProductEntity> products = productService.findAll();
        List<ProductTypeEntity> ProductTypes = productTypeService.findAll();

        listProductsPageBean.setFilteredProducts(ProductMapper.INSTANCE.toDTOList(filteredProducts));

        listProductsPageBean.setProducts(ProductMapper.INSTANCE.toDTOList(products));

        listProductsPageBean.setProductTypes(ProductTypeMapper.toDTOList(ProductTypes));

        req.getRequestDispatcher("WEB-INF/ConfProducts.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.setAttribute("manipulateFilter", Boolean.TRUE);

        String productIdString = request.getParameter("manipulateFilter");
        Long productId = Long.parseLong(productIdString);

        ListProductsPageBean listProductsPageBean = (ListProductsPageBean) request.getSession()
                .getAttribute(LIST_PRODUCTS_PAGE_BEAN);

        if (listProductsPageBean == null) {
            listProductsPageBean = new ListProductsPageBean();
            request.getSession().setAttribute(LIST_PRODUCTS_PAGE_BEAN, listProductsPageBean);
        }

        if (listProductsPageBean.getProductIdsForFilters().contains(productId)) {
            listProductsPageBean.removeId(listProductsPageBean.getProductIdsForFilters(), productId);
            listProductsPageBean.removeProductById(listProductsPageBean.getSelectedProducts(), productId);
        } else {
            listProductsPageBean.addProductIdForFilters(productId);
            ProductDTO product = listProductsPageBean.getProductById(listProductsPageBean.getProducts(), productId);
            listProductsPageBean.addToselectedProducts(product);

            List<ProductEntity> filteredProducts = productService.findFiltered(listProductsPageBean.getProductIdsForFilters());
            listProductsPageBean.setFilteredProducts(ProductMapper.INSTANCE.toDTOList(filteredProducts));
        }

        response.sendRedirect(request.getContextPath() + "/ConfProducts");
    }

}
