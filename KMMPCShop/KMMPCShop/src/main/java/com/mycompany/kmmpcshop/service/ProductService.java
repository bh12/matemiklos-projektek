
package com.mycompany.kmmpcshop.service;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import com.mycompany.kmmpcshop.dao.ProductDAO;
import com.mycompany.kmmpcshop.entity.ProductEntity;

@Singleton
@TransactionAttribute
public class ProductService
{
    @Inject
    private ProductDAO productDAO;

    public List<ProductEntity> findAll() {
        return productDAO.findAll();
    }
    
    public List<ProductEntity> findFiltered(List<Long> filterIds) {
        return productDAO.findFiltered(filterIds);
    }

    
}

