/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.service;

import com.mycompany.kmmpcshop.dao.FilterDAO;
import com.mycompany.kmmpcshop.entity.FilterEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author MMD
 */
@Singleton
@TransactionAttribute
public class FilterService {

    @Inject
    private FilterDAO filterDAO;

    public List<FilterEntity> findAll() {
        return filterDAO.findAll();
    }
}
