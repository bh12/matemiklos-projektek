/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kmmpcshop.mapper;

import com.mycompany.kmmpcshop.dto.ProductTypeDTO;
import com.mycompany.kmmpcshop.entity.ProductTypeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductTypeMapperInterface {
    
    ProductTypeMapperInterface INSTANCE = Mappers.getMapper(ProductTypeMapperInterface.class);
    
    ProductTypeEntity toEntity(ProductTypeDTO productTypeDTO);
}
