package hu.mmd.rd;

import java.util.ArrayList;

/**
 *
 * @author MMD
 */
public class Profit {

    public static int getMaxProfit(ArrayList<Integer> stocks) {
       
        ArrayList<Integer> profits = Profit.getProfits(stocks);
        int maxProfit = Profit.getMax(profits);
        
        return maxProfit;
        
    }

    public static ArrayList<Integer> getProfits(ArrayList<Integer> stocks) {

        ArrayList<Integer> profits = new ArrayList<>();

        for (int i = 0; i < stocks.size(); i++) {

            int stock = stocks.get(i) * -1;
            int profit = 0;

            for (int j = 0; j < stocks.size(); j++) {
                if((j+i) < stocks.size()){
                profit = stock + stocks.get(j+i);
                profits.add(profit);
                }
            }

        }
        return profits;
    }

    public static int getMin(ArrayList<Integer> list) {

        int min = list.get(0);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) < min) {
                min = list.get(i);
            }
        }

        return min;
    }

    public static int getMax(ArrayList<Integer> list) {

        int max = list.get(0);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max) {
                max = list.get(i);
            }
        }

        return max;
    }

}
