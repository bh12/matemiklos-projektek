package hu.mmd.rd;

import java.util.ArrayList;

/**
 *
 * @author MMD
 */
public class RDMain {

    public static void main(String[] args) {
        
        ArrayList<Integer> stocks = new ArrayList<>();
        stocks.add(30);
        stocks.add(5);
        stocks.add(14);
        stocks.add(23);
        
        System.out.println(Profit.getMaxProfit(stocks));
        
        String number = "1011";
        System.out.println(StringBinary.numberOfpointlessOperations(number));
        String number2 = "11";
        System.out.println(StringBinary.numberOfpointlessOperations(number2));
    }
    
}
