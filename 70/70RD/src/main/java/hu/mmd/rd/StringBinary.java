package hu.mmd.rd;

/**
 *
 * @author MMD
 */
public class StringBinary {

    public static int numberOfpointlessOperations(String number) {

        int counter = 0;

        int decimal = Integer.parseInt(number, 2);

        while (decimal > 0) {
            if (decimal % 2 == 0) {
                decimal = decimal / 2;
                counter++;
            } else {
                decimal = decimal - 1;
                counter++;
            }
        }

        return counter;
    }
}
