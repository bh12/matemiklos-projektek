package hu.mmd.clientmain;

import hu.mmd._ejbgyak.HelloEJBRemote;
import hu.mmd._ejbgyak.HelloRemote;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author MMD
 */
public class Main {

    public static void main(String[] args) {

        Properties environment = new Properties();
        environment.put(Context.INITIAL_CONTEXT_FACTORY, "fish.payara.ejb.rest.client.RemoteEJBContextFactory");
        environment.put(Context.PROVIDER_URL, "http://localhost:8080/ejb-invoker");

        try
        {
            InitialContext ejbRemoteContext = new InitialContext(environment);
            HelloEJBRemote hr = (HelloEJBRemote) ejbRemoteContext.lookup("java:global/49_EJBGyak-1.0-SNAPSHOT/HelloRemote!hu.mmd._ejbgyak.HelloEJBRemote");
            hr.sayHello();
        } catch (NamingException e)
        {
            e.printStackTrace();
        }
        
    }
    
}
