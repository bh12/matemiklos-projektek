package hu.mmd._ejbgyak;

import javax.ejb.Singleton;
import javax.ejb.LocalBean;

/**
 *
 * @author MMD
 */
@Singleton
@LocalBean
public class Hello implements HelloEJBLocal {

    public static void main(String[] args) {
        Hello h = new Hello();
        h.sayHello();
    }

    @Override
    public void sayHello() {
        System.out.println("Hello EJBWord");
    }

}
