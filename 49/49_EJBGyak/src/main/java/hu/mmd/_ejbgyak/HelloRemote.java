package hu.mmd._ejbgyak;

import javax.ejb.Remote;
import javax.ejb.Singleton;

/**
 *
 * @author MMD
 */
@Singleton
@Remote
public class HelloRemote implements HelloEJBRemote{

    @Override
    public void sayHello() {
        System.out.println("Hello from EJBRemote");
    }

}
