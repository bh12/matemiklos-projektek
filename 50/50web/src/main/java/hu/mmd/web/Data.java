package hu.mmd.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MMD
 */
@WebServlet(urlPatterns = {"/Data"})
public class Data extends HttpServlet {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/50DB";
    private static final String INSERT_STATEMENT = "INSERT INTO Data "
            + "(Name, Email, Date of birth) "
            + "VALUES(?,?,?);";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Data</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Data at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                try (Connection conn = DriverManager.getConnection(CONNECTION_URL, "MMD", "mmdmmd2");
                PreparedStatement ps = conn.prepareStatement(INSERT_STATEMENT);) {

            Class.forName("com.mysql.cj.jdbc.Driver");

            String name = request.getParameter("Name");
            String email = request.getParameter("Email");
            String date = request.getParameter("Date");


            ps.setString(1, name);
            ps.setString(2, email);
            ps.setString(3, date);

            System.out.println("Hany sor insertalodott?" + ps.executeUpdate());

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Data.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("/Data");
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
