CREATE DATABASE IF NOT EXISTS messagesDB;
USE messagesDB;

create table messages (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email varchar(128),
    subject varchar(128),
    message text(128)
);

CREATE DATABASE IF NOT EXISTS library2;
USE library2;

create table author (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(128),
    book_id INT
);

create table book (
    id INT AUTO_INCREMENT PRIMARY KEY,
    isbn varchar(128),
    title varchar(128),
    description varchar(128),
    author_id INT,
    FOREIGN KEY (author_id) REFERENCES author(id)
);