package com.mycompany.bh12library.service;

import com.mycompany.bh12library.dao.BookDAO;
import com.mycompany.bh12library.entity.BookEntity;
import com.mycompany.bh12library.exception.book.WrongBookTitleException;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import java.util.List;

@Singleton
@TransactionAttribute
public class BookService
{
    @Inject
    private BookDAO bookDAO;

    public List<BookEntity> findAll() {
        return bookDAO.findAll();
    }

    public void save(BookEntity bookEntity) {
        // ez egy tiltott konyv
        if (bookEntity.getTitle().contains("utca")) {
            throw new WrongBookTitleException("Hibas cimet adtal meg a konyhoz");
        }

        bookDAO.save(bookEntity);
    }
}
