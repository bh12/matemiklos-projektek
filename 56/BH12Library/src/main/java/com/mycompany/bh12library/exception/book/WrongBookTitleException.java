package com.mycompany.bh12library.exception.book;

public class WrongBookTitleException extends RuntimeException
{
    public WrongBookTitleException(String message)
    {
        super(message);
    }
}
