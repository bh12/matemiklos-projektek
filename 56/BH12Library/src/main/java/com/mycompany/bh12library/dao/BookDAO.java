package com.mycompany.bh12library.dao;

import com.mycompany.bh12library.dto.BookFilterDTO;
import com.mycompany.bh12library.entity.BookEntity;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Singleton
@TransactionAttribute
public class BookDAO {

    private static final String QUERY_FIND_BY_FILTER = "SELECT b FROM bookEntity b";

    @PersistenceContext
    private EntityManager em;

    public List<BookEntity> findAll() {
        return em.createNamedQuery(BookEntity.QUERY_FIND_ALL, BookEntity.class).getResultList();
    }

    public void save(BookEntity bookEntity) {
        em.persist(bookEntity);
    }

    public List<BookEntity> findByFilter(BookFilterDTO Filer) {

        return findAll();

    }
}
