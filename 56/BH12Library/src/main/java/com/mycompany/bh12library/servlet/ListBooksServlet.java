package com.mycompany.bh12library.servlet;

import com.mycompany.bh12library.bean.ListBooksPageBean;
import com.mycompany.bh12library.dto.BookFilterDTO;
import com.mycompany.bh12library.entity.BookEntity;
import com.mycompany.bh12library.mapper.BookMapper;
import com.mycompany.bh12library.service.BookService;
import org.mapstruct.factory.Mappers;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/listBooks")
public class ListBooksServlet extends HttpServlet {

    private static final String LIST_BOOKS_PAGE_BEAN = "listBooksPageBean";

    @Inject
    private BookService bookService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        BookFilterDTO bookFilterDTO = new BookFilterDTO();

        bookFilterDTO.setIsbn(req.getParameter("isbn"));
        bookFilterDTO.setTitle(req.getParameter("title"));
        bookFilterDTO.setDescription(req.getParameter("descripton"));
        bookFilterDTO.setAuthorName(req.getParameter("authorName"));

        List<BookEntity> books = bookService.findAll();
        ListBooksPageBean listBooksPageBean = (ListBooksPageBean) req.getSession()
                .getAttribute(LIST_BOOKS_PAGE_BEAN);

        if (listBooksPageBean == null) {
            listBooksPageBean = new ListBooksPageBean();
            req.getSession().setAttribute(LIST_BOOKS_PAGE_BEAN, listBooksPageBean);
        }

        listBooksPageBean.setBooks(BookMapper.INSTANCE.toDTOList(books));

        req.getRequestDispatcher("WEB-INF/listBooks.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<BookEntity> books = bookService.findAll();
        ListBooksPageBean listBooksPageBean = (ListBooksPageBean) req.getSession()
                .getAttribute(LIST_BOOKS_PAGE_BEAN);

        if (listBooksPageBean == null) {
            listBooksPageBean = new ListBooksPageBean();
            req.getSession().setAttribute(LIST_BOOKS_PAGE_BEAN, listBooksPageBean);
        }

        listBooksPageBean.setBooks(BookMapper.INSTANCE.toDTOList(books));

        resp.sendRedirect(req.getContextPath() + "/listBooks");
    }
}
