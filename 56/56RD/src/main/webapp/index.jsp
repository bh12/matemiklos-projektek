<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome orders and products!</h1>
    <a href="${pageContext.request.contextPath}/ListJSP">List</a>
    
    <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Order Date</th>
                    <th scope="col">Delivery Date</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${DAO.GetOrders()}" var="OrderDTO">
                <tr>
                    <th scope="row">${OrderDTO.id}</th>
                    <td>${OrderDTO.orderDate}</td>
                    <td>${OrderDTO.deliveryDate}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    </body>
</html>
