package builder;

/**
 *
 * @author MMD
 */
public class Account {

    private String name;
    private String accountNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public static class Builder {
        private String name;
        private String accountNumber;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }
        
        public Account build() {
            Account account = new Account();
            account.setName(name);
            account.setAccountNumber(accountNumber);
            return account;
        }
    }

}
