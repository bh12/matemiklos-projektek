package builder;

/**
 *
 * @author MMD
 */
public class App {
    
    public static void main(String[] args) {
        Account ac = new Account.Builder()
                .setName("XY")
                .setAccountNumber("1")
                .build();
    }
    
}
