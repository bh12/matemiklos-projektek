package hu.mmd;

/**
 *
 * @author MMD
 */
public class App {

    public static void main(String[] args) {
        ExpensiveObjProxy proxy = new ExpensiveObjProxy();
        callProcess(proxy);
    }
    
    public static void callProcess(ExpensiveObjInterface obj){
        obj.process();
    }
}
