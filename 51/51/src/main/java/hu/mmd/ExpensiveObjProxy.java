package hu.mmd;

/**
 *
 * @author MMD
 */
public class ExpensiveObjProxy implements ExpensiveObjInterface {

    private ExpensiveObj obj;

    @Override
    public void process() {

        if (obj == null) {
            obj = new ExpensiveObj();
        }

        obj.process();

    }
}
