package singleton;

/**
 *
 * @author MMD
 */
public class MySingleton {

    private static final MySingleton INSTANCE = new MySingleton();

    private MySingleton() {
        System.out.println("Something");
    }

    public static MySingleton getINSTANCE() {

        return INSTANCE;
    }

}
