package singleton;

/**
 *
 * @author MMD
 */
public class MyLazySingleton {

    private static MyLazySingleton INSTANCE;

    private MyLazySingleton() {
        System.out.println("Something");
    }

    public static MyLazySingleton getINSTANCE() {

        if (INSTANCE == null) {
            INSTANCE = new MyLazySingleton();
        }

        return INSTANCE;
    }

}
