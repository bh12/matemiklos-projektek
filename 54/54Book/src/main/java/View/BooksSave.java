package View;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MMD
 */
@WebServlet(name = "BooksSave", urlPatterns = {"/BooksSave"})
public class BooksSave extends HttpServlet {

    public BookDTO bookDTO;
    public AuthorsDTO autDTO;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BooksSave</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BooksSave at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("AddBook.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Long Id = Long.valueOf(request.getParameter("Id"));
        String description = request.getParameter("description");

        String authors = request.getParameter("authors");
        String[] authorsArray = authors.split(",");
        List<String> authorsStringList = new ArrayList<>();
        authorsStringList = Arrays.asList(authorsArray);
        
        
        
        bookDTO = new BookDTO(Id, description);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
