package View;

import Model.AuthorsEntity;
import java.util.List;

/**
 *
 * @author MMD
 */
public class BookDTO {
     private Long id;
     private String description;
     private List<AuthorsEntity> aes;

    public BookDTO(Long id, String description, List<AuthorsEntity> aes) {
        this.id = id;
        this.description = description;
        this.aes = aes;
    }

    public List<AuthorsEntity> getAes() {
        return aes;
    }

    public void setAes(List<AuthorsEntity> aes) {
        this.aes = aes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
          
}
