package pkg14.hf;

public class Apple extends Fruit{
    
    protected String taste;

    public Apple(int price, String colour) {
        super(price, colour);
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    @Override
    public String toString() {
        return "Fruit: " + "price = " + price + "Ft/" + unit +
                ", colour = " + colour + ", and taste like " + this.taste;
        
    }
    
    
    
}
