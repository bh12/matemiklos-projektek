package pkg14.hf;

public class Orange extends Fruit{
    
    
    private final String ORANGE_COLOUR = "Orange";

    //Csak úgy sikerült a narancs, hogy beraktam egy üres konstruktort is a Friut-ba.
    
    public Orange(int price) {
        super(price,"Orange");
        
        this.price = price;
        this.colour = ORANGE_COLOUR;
        this.unit = KG;
        
    }



        
    
    
    
}
