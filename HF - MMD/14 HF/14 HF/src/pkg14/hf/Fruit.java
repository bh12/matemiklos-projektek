package pkg14.hf;

public class Fruit {
    
    protected int price;
    protected String colour;
    protected String unit;
    
    protected final String KG = "kg";

    public Fruit(int price, String colour) {
        this.price = price;
        this.colour = colour;
        this.unit = KG;
    }
    
//    public Fruit() { //Csak úgy sikerült a narancs, hogy beraktam egy üres konstruktort is a Friut-ba.
//        this.price = price;
//        this.colour = colour;
//        this.unit = KG;
//    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return "Fruit: " + "price = " + price + "Ft/" + unit+ ", colour = " + colour;
    }
    
    
    
    
}
