package pkg14.hf;
public class HF {

    public static void main(String[] args) {
        
        Fruit fruit1 = new Fruit(100, "Yellow");
        
        System.out.println(fruit1);
        
        //Csak úgy sikerült a narancs, hogy beraktam egy üres konstruktort is a Friut-ba.
        
        Banana banana1 = new Banana(200, "Yellow");
        
        System.out.println(banana1);        
        
        Apple apple1 = new Apple(120, "Red");
        
        apple1.setTaste("poison");
        
        System.out.println(apple1);
        
        Orange orange1 = new Orange(200);
        
        System.out.println(orange1);
        
    }
    
}
