CREATE DATABASE IF NOT EXISTS HF36_library;

USE HF36_library;

DROP TABLE IF EXISTS book;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS rent;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS switchBoard_bookAuthor;

CREATE TABLE IF NOT EXISTS author (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    country VARCHAR(127)
);

CREATE TABLE IF NOT EXISTS switchBoard_bookAuthor (
    id INT AUTO_INCREMENT PRIMARY KEY,
    author_id INT,
    FOREIGN KEY (author_id)
        REFERENCES author (id)
);

CREATE TABLE IF NOT EXISTS category (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(127)
);

CREATE TABLE IF NOT EXISTS book (
	id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(511) NOT NULL,
    year_of_issue INT,
    description TEXT,
    bookAuthor_id INT,
    category_id INT,
    rentable BIT,
    max_rent_time VARCHAR(128),
    FOREIGN KEY (bookAuthor_id) REFERENCES switchboard_bookAuthor(id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS user (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255),
    id_number INT,
    date_of_birth DATE,
    adress VARCHAR(255),
    phone_number VARCHAR(63)
);