package banksimulation;


import banksimulation.exceptions.BankingException;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.ArrayList;
import java.util.List;

public class Bank {
    
    private String bankName;
    
    private int CLIENT_BASE_BALANCE;
    
    private int MAX_TRANSFER_AMMOUNT;
    
    private List<Client> clients = new ArrayList<>();
    private List<Transfer> transfers = new ArrayList<>();
    private List<BankingException> bankingExceptions = new ArrayList<>();

    public Bank(String bankName, int CLIENT_BASE_BALANCE, int MAX_TRANSFER_AMMOUNT) {
        this.bankName = bankName;
        this.CLIENT_BASE_BALANCE = CLIENT_BASE_BALANCE;
        this.MAX_TRANSFER_AMMOUNT = MAX_TRANSFER_AMMOUNT;
    }

    public Bank(String bankName) {
        this.bankName = bankName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getCLIENT_BASE_BALANCE() {
        return CLIENT_BASE_BALANCE;
    }

    public void setCLIENT_BASE_BALANCE(int CLIENT_BASE_BALANCE) {
        this.CLIENT_BASE_BALANCE = CLIENT_BASE_BALANCE;
    }

    public int getMAX_TRANSFER_AMMOUNT() {
        return MAX_TRANSFER_AMMOUNT;
    }

    public void setMAX_TRANSFER_AMMOUNT(int MAX_TRANSFER_AMMOUNT) {
        this.MAX_TRANSFER_AMMOUNT = MAX_TRANSFER_AMMOUNT;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Transfer> transfers) {
        this.transfers = transfers;
    }

    public List<BankingException> getBankingExceptions() {
        return bankingExceptions;
    }

    public void setBankingExceptions(List<BankingException> bankingExceptions) {
        this.bankingExceptions = bankingExceptions;
    }

    @Override
    public String toString() {
        return "Bank{" + "bankName=" + bankName + '}';
    } 
    
}
