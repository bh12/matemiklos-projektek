package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileDataLoader implements DataLoader {

    @Override
    public List<Client> loadInitialData() {
        List<Client> clients = new ArrayList<>();
        File clientFile = new File("clients.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");

                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);

                Client c = new Client(clientId, accountNumber, balance);

                clients.add(c);

                line = br.readLine();
            }

        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
        
        return clients;
        
    }
    
        public List<Transfer> loadTransferData(List<Client> clients) {
            
        List<Transfer> transfers = new ArrayList<>();
        File clientFile = new File("transfers.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] transferArray = line.split(",");

                String clientSourceSt = transferArray[0];
                String clientTargetSt = transferArray[1];
                long ammount = Long.parseLong(transferArray[2]);
                
                Client cs = new Client(clientSourceSt, "", 0);
                Client ct = new Client(clientTargetSt, "", 0);
                
                Client clientSource = clients.get(clients.indexOf(cs));
                Client clientTarget = clients.get(clients.indexOf(ct));

                Transfer t = new Transfer(clientSource, clientTarget, ammount);
                
                
                transfers.add(t);

                line = br.readLine();
            }

        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
        
        return transfers;
        
    }

}
