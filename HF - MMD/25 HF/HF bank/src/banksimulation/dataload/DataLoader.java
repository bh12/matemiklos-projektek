
package banksimulation.dataload;

import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.List;

public interface DataLoader {
    
    public List<Client> loadInitialData();

    public List<Transfer> loadTransferData(List<Client> clients);
    
}
