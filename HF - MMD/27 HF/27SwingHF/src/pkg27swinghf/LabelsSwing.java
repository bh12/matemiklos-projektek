package pkg27swinghf;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;

public class LabelsSwing extends JFrame implements ActionListener {

    private JButton button1;

    private JTextArea text1;
    private JTextArea text2;

    private Integer resultValue;
    private Dimension dim = new Dimension(10, 20);

    public LabelsSwing() {

        this.button1 = new JButton("Text fájl betöltése");

    }

    public void swingProgram() {

        JFrame f = new JFrame();

        f.setSize(400, 500);
        f.setVisible(true);
        f.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        button1.setMaximumSize(this.dim);
        
        f.add(button1);

        JFileChooser jc = new JFileChooser(new File("D:\\Dokumentumok\\Programozás\\codes\\matemiklos-projektek"));


        button1.addActionListener((e) -> {
        jc.showSaveDialog(null);
        });

        f.setLayout(new FlowLayout(FlowLayout.CENTER));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
