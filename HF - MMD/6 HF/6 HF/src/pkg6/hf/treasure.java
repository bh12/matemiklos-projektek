
package pkg6.hf;
import java.util.Scanner;
public class treasure {
    
public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    int fieldMSide = askfieldSide("Magasság");
    int fieldNSide = askfieldSide("Szélesség");
    
    int stepN = 0;
    int stepM = 0;
    
    int [] [] mineField = new int [fieldMSide] [fieldNSide];

    mineField = generateMines(fieldMSide,fieldNSide,2,4);
    printMultiArray(mineField);
    
    game(mineField);
    

}

public static void minesAround (int positionN, int positionM, int [][] minefield) {

    int counter = 0;
    
    for (int i = -1; i < 2; i++){
        for (int x = -1; x < 2; x++){
            if ((positionN + i) >= 0 && (positionM + x) >= 0 
                && (positionN + i) < minefield.length && (positionM + x) < minefield[1].length    
                  ){
                counter = counter + minefield [positionN + i][positionM + x];
            }
        }
    }
    
    if (counter < 2){
        System.out.println("Hideg a környék");
    }else if (counter > 1 && counter < 5){
        System.out.println("Langyos a környék");
    }else {
        System.out.println("Forró a környék");
    }
    

}

public static int step (int [][] minefield, String dimension, int position) {
    Scanner sc = new Scanner(System.in);
    
    int step = 0;
    
    do{
    System.out.println("Lépjen (" + dimension + ")");
    step = sc.nextInt()+position;
    if(step < 0){
        System.out.println("Nulla és egy között adjon meg számot");
    }
    }while (step < 0 && step > 1);
    
    return step;
    
}



    public static void game(int[][] minefield) {

        Scanner sc = new Scanner(System.in);
        int stepN = 0;
        int stepM = 0;
        boolean life = true;
        
        int [] position = {0,0};
        
        int treasures = sumMultiArray(minefield);
        
        int survivedCounter = 0;

        do { 
            stepN = stepN + step(minefield, "Függőlegesen", stepN);
            stepM = stepM + step(minefield, "Vízszintesen", stepM);

            if (checkStep(stepN, stepM, minefield)) {
                System.out.println("Kincs!");
                treasures--;

                if (treasures < 1) {
                    life = false;
                    System.out.println("Nyertél");
                    System.out.println("Lépések száma: " + survivedCounter);
                }
                
                if (life) {
                    survivedCounter++;
                    minesAround(stepN, stepM, minefield);
                    System.out.println("Pozíció: " + stepN + "," + stepM);
                }                
                
            } else {
                System.out.println("Itt nincs kincs");
                survivedCounter++;
                minesAround(stepN, stepM, minefield);
                System.out.println("Pozíció: " + stepN + "," + stepM);
            }
        } while (life);

    }

    public static int askfieldSide(String side) {
        Scanner sc = new Scanner(System.in);

        int fieldSide = 0;

        do {
            System.out.println("Adja meg a játékmező oldalát (" + side + ")");
            fieldSide = sc.nextInt();
            if (fieldSide < 0) {
                System.out.println("A megadott szám legyen nagyobb mint 0");
            }
        } while (fieldSide < 0);

        return fieldSide;
    }


public static int [][] generateMines(int fieldMSide, int fieldNSide, int maxPercent, int minPercent) {
    
    int [] [] mineField = new int [fieldMSide] [fieldNSide];
    int max = (fieldMSide*fieldNSide)/maxPercent;
    int min = (fieldMSide*fieldNSide)/minPercent;
    
    
    int numberOfMines = (int) (Math.random()*(max-min+1)+min);
    
    int randomnumber1 = 0;
    int randomnumber2 = 0;
    
        for (int i = 0; i < numberOfMines; i++) {
            randomnumber1 = getRandomNumber(0,fieldMSide-1);
            randomnumber2 = getRandomNumber(0,fieldNSide-1);
            
            if (mineField [randomnumber1][randomnumber2] == 1){
                i--;
            }
            mineField [randomnumber1][randomnumber2] = 1;}
           
        
        
        return mineField;
    
}

    public static int getRandomNumber(int min, int max) {
        return (int) (Math.random() * (max - min + 1)) + min;
    }
    
    public static void printMultiArray(int[][] multiArray) {
        for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                System.out.print(multiArray[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public static boolean checkStep(int n, int m, int[][] multiArray) {
        
        boolean blowUp = false;
        
        if (multiArray [n][m] == 1){
        blowUp = true;}
        
        return blowUp; 
    }
    
            public static int sumMultiArray(int[][] multiArray) {
        
            int sumarray = 0;
            
            for (int i = 0; i < multiArray.length; i++) {
            for (int j = 0; j < multiArray[i].length; j++) {
                sumarray = sumarray + multiArray[i][j];
            }
        } return sumarray;
    } 
    

}


