package pkg21.hf.fájlkezelés;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
public class HFFájlkezelés6 {

    public static int[] lotteryNumbers = new int[5];

    public static void main(String[] args) {
        File f = new File("lotto.txt");

        Scanner sc = new Scanner(System.in);
        
        System.out.println("Adja meg a lottószelvények számát");
        int ticketsNumber = sc.nextInt();

        try {
            FileWriter writer = new FileWriter(f);
            int[] lotteryNumbers = new int[5];

            for (int i = 0; i < ticketsNumber; i++) {
                for (int j = 0; j < 5; j++) {
                    lotteryNumbers[j] = randLotteryNumb(i);                    
                }
                for (int k = 0; k < 5; k++) {
                    writer.write(lotteryNumbers[k] + "\n");
                }
            }
            writer.close();
        } catch (IOException ex) {
            System.out.println("Hiba történt");
        }


        try (FileReader reader = new FileReader(f)) {

            int charNum = reader.read();
            do {
                char c = (char) charNum;
                System.out.print(c);
                charNum = reader.read();

            } while (charNum != -1);
            reader.close();
        } catch (IOException e) {
            System.out.println("Hiba történt");
        }

    }

    public static int randLotteryNumb(int number) {
        boolean repeat;
        int rnd;
        do {
            repeat = true;
            rnd = (int) (Math.random() * (90) + 1);
            for (int i = 0; i < number; i++) {
                if (lotteryNumbers[i] == rnd) {
                    repeat = false;
                    break;
                }
            }
        } while (!repeat);
        return rnd;
    }

}

