package pkg21.hf.fájlkezelés;
import java.io.File;
import java.io.FileWriter;
import java.util.*;
public class HFFájlkezelés1 {

    public static void main(String[] args) throws Exception{
        

        File f = new File("testText.txt");
        
        
        FileWriter fw = new FileWriter(f);
        
        fw.write("Hello World!");
        fw.close();
        
        System.out.println(f.isFile());
        System.out.println(f.getPath());
        System.out.println(f.getAbsolutePath());
        System.out.println(f.getParentFile());
        System.out.println(f.getName());
        System.out.println(f.length());
        System.out.println(f.canRead());
        System.out.println(f.canWrite());
        System.out.println(f.isHidden());
        System.out.println(f.lastModified());
    }
    
}
