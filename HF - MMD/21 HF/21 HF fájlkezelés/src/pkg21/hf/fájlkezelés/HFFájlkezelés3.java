package pkg21.hf.fájlkezelés;
import java.io.File;
import java.io.FileWriter;
import java.util.*;
public class HFFájlkezelés3 {

    public static void main(String[] args) throws Exception {

        Scanner sc = new Scanner(System.in);
        File f = new File("scanner_text.txt");
        
        try{
        FileWriter fw = new FileWriter(f);
        
        String line = "X";
        int counter = 1;

        while(!line.equals(" ")){
            line = sc.nextLine();
            fw.write(counter + ". " + line + "\r");
            
            counter++;
        }
        
        fw.close();
        
        }catch(Exception e){
            
        }
    }
    
}
