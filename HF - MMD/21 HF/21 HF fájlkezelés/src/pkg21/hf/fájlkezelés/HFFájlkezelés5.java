package pkg21.hf.fájlkezelés;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
public class HFFájlkezelés5 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String sourceFile = sc.nextLine();
        String targetPath = sc.nextLine();

        System.out.println(sourceFile);
        System.out.println(targetPath);

        File f = new File(targetPath + "new_text.txt");

        try {
            BufferedReader br = new BufferedReader(new FileReader(sourceFile));
            FileWriter fw = new FileWriter(f, true);
            String text = br.readLine();

            while (text != null) {
                System.out.println(text);
                fw.write(text);
                text = br.readLine();
            }
            
            

            br.close();
            fw.close();

        } catch (Exception e) {
            System.out.println("Hiba történt");
        }
        
        
    }
    
}
