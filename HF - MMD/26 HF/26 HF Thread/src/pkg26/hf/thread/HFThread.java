package pkg26.hf.thread;

public class HFThread {

    public static void main(String[] args) {
        
        readAndStorageThread ras = new readAndStorageThread();
        writerThread wt = new writerThread(ras.getStorageQue());
        
        Thread t1 = new Thread(ras);
        Thread t2 = new Thread(wt);
        
        t2.setDaemon(true);
        t2.setDaemon(true);
        
        t1.start();
        t2.start();
        
 
        try{
        t1.join();
        }catch (InterruptedException e){
            System.out.println("Hiba történt a join parancsnál");
        }
    }
    
}
