package pkg26.hf.thread;

import java.util.PriorityQueue;
import java.util.Scanner;

public class readAndStorageThread implements Runnable {

    private PriorityQueue<String> storageQue;
    private static boolean readingOn;
    private static boolean readingStart;

    public readAndStorageThread() {

        this.storageQue = new PriorityQueue<String>();
        this.readingOn = true;
    }

    @Override
    public void run() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Gépelje a szöveget");
        String line = sc.nextLine();

        while (!"".equals(line)) {

            synchronized (storageQue) {

                storageQue.add(line);
            }

            line = sc.nextLine();

            if (" ".equals(line)) {
                this.readingOn = false;
            }

        }

    }

    public PriorityQueue<String> getStorageQue() {
        return storageQue;
    }

    public void setStorageQue(PriorityQueue<String> storageQue) {
        this.storageQue = storageQue;
    }

    public static boolean isReadingOn() {
        return readingOn;
    }

    public static void setReadingOn(boolean readingOn) {
        readAndStorageThread.readingOn = readingOn;
    }

}
