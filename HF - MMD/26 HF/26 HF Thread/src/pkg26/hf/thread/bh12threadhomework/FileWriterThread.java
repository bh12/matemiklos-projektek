/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg26.hf.thread.bh12threadhomework;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Queue;

/**
 *
 * @author kopacsi
 */
public class FileWriterThread extends Thread {

    private final Queue<String> buffer;

    public FileWriterThread(Queue<String> buffer) {
        this.buffer = buffer;
    }
    
    @Override
    public void run() {
        try (FileWriter fw = new FileWriter("output.txt"))
        {
            while (!Thread.currentThread().isInterrupted()) {
                synchronized(buffer) {
                    if (buffer.isEmpty()) {
                        //System.out.println("Im waiting for input");
                        continue;
                    }
                    String line = buffer.poll();
                    fw.write(line);
                    fw.flush();
                }
            }
        } catch (IOException ex) {
            System.out.println("Unexpected error occurred:" + ex.getLocalizedMessage());
        }
    }
    
}
