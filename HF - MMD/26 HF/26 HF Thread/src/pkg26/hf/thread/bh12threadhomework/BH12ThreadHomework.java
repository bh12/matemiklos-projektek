/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg26.hf.thread.bh12threadhomework;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 *
 * @author kopacsi
 */
public class BH12ThreadHomework {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final Queue<String> buffer = new LinkedList<>();
        FileWriterThread t = new FileWriterThread(buffer);
        t.start();
        
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        while (!"".equals(line)) {
            synchronized(buffer) {
                buffer.add(line);
            }
            line = sc.nextLine();
        }
        t.interrupt();
        
    }
    
}
