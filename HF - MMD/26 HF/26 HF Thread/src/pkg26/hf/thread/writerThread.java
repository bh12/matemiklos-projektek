package pkg26.hf.thread;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.PriorityQueue;

public class writerThread implements Runnable {

    private PriorityQueue<String> storageQue;

    public writerThread(PriorityQueue storageQue) {
        this.storageQue = storageQue;
    }

    @Override
    public void run() {

        File f = new File("ThreadWriterResult.txt");

        try {
            FileWriter fw = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(fw);

            String line = null;
            StringBuilder stb = new StringBuilder();

            while (readAndStorageThread.isReadingOn()) {

                synchronized (storageQue) {
                    line = storageQue.poll();
                }

                if (line != null) {
//                    stb.append(line + System.lineSeparator());
                    fw.write(line + System.lineSeparator());
                    fw.flush();
                }

            }

//            line = stb.toString();
//            bw.write(line);
            bw.close();

        } catch (IOException e) {
            System.out.println("Hiba történt a fájlírás részben");
        }

    }

    public PriorityQueue<String> getStorageQue() {
        return storageQue;
    }

}
