package pkg8.hf;


public class HFsorozat {
    
    public static void main(String[] args) {
        
        int [] array = fillRandomArray(10,0,5);
        
        printArray(array);
        
        System.out.println(formAscending (array));

    }
    
    public static void printArray(int[] Array) {
        for (int i = 0; i < Array.length; i++) {
            System.out.print(Array[i]);

            if (i < Array.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
    }

    public static int[] fillRandomArray(int size, int min, int max) {

        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max - min + 1) + min);
        }
        return array;
    }
    
    
    public static int formAscending (int[] array){
        
        boolean fromAscOk;
        int i = 0;
        int from = array[i];
        
        do{           
            fromAscOk = true;
            
            for (int j = i; j < (array.length); j++){
                if(array[i] > array[j]){
                fromAscOk = false;
                i++;
                }
                
                from = array[i];
            }
            
        }while (fromAscOk && i > array.length);
        
        return from;
        
    }
}
