package pkg8.hf;
import java.util.Scanner;
public class Játék {
    //karakterek
    public static final char EMPTY_CHAR = '.';
    public static final char PLAYER_CHAR = 'i';
    public static final char GATE_CHAR = 'O';
    public static final char MINE_CHAR = 'x';
    
    public static final char UP_CHAR = 'w';
    public static final char DOWN_CHAR = 's';
    public static final char RIGHT_CHAR = 'd';
    public static final char LEFT_CHAR = 'a';
    public static final char EXIT_CHAR = 'x';
    
    //helyek
    public static int playerX;
    public static int playerY;
    
    public static int gateX;
    public static int gateY;
    
    public static final int EMPTY = 0;
    public static final int PLAYER = 1;
    public static final int GATE = 2;
    public static final int MINE = 3;
    
    //számok
    public static final int FIELD_SIZE = 10;
    public static final int NUMBER_OF_MINES = 10;
    
    //objektumok
    public static int [][] field = new int[FIELD_SIZE][FIELD_SIZE];
    
    //feltételek
    
    public static boolean win = false;
    public static boolean loose = false;
    public static boolean game = true;

    public static void main(String[] args) {
        
        initMap ();
        game ();

    }
    
    public static void initMap (){
        do{
        playerX = randNumber(0,FIELD_SIZE-1);
        playerY = randNumber(0,FIELD_SIZE-1);
        
        field [playerX][playerY] = PLAYER;
        
        gateX = randNumber(0,FIELD_SIZE-1);
        gateY = randNumber(0,FIELD_SIZE-1);
        
        field [gateX][gateY] = GATE;
        
        }while(PLAYER == GATE);
        
        int mineX = 0;
        int mineY = 0;
        
        for (int i = 0; i < NUMBER_OF_MINES; i++){
            
        mineX = randNumber(0,FIELD_SIZE-1);
        mineY = randNumber(0,FIELD_SIZE-1);
        
        if (field [mineX][mineY] == EMPTY){
            field [mineX][mineY] = MINE;
        }else {i--;}
            
        }
        
    }
    
    public static void printField (){
        
        char [][] fieldChar = new char [FIELD_SIZE][FIELD_SIZE];
        
        
        for(int i = 0; i < FIELD_SIZE; i++){
            for(int j = 0; j < FIELD_SIZE; j++){
                if (field[i][j] == EMPTY) {
                    fieldChar[i][j] = EMPTY_CHAR;
                }
                if (field[i][j] == PLAYER) {
                    fieldChar[i][j] = PLAYER_CHAR;
                }
                if (field[i][j] == GATE) {
                    fieldChar[i][j] = GATE_CHAR;
                }
                if (field[i][j] == MINE) {
                    fieldChar[i][j] = MINE_CHAR;
                }     
            }
        }
        
        for(int i = 0; i < FIELD_SIZE; i++){
            for(int j = 0; j < FIELD_SIZE; j++){
                    System.out.print("");
                System.out.print(fieldChar[i][j]);
            }
            System.out.println();
        }
        
    }
    
    public static int randNumber (int min, int max){
        int randNumber = (int)(Math.random()*(max-min)+min);
        
        return randNumber;
    }
    
    public static void move (){
                
        char step;
        Scanner sc = new Scanner(System.in);
        
        do{
                
        System.out.println("Lépjen (w,s,d,a (x)- föl, le jobb, balra (kilép)");
        
        step = sc.next().charAt(0);        
        
        switch (step){
            case UP_CHAR: handleMove(playerX-1, playerY); break;
            case DOWN_CHAR: handleMove(playerX+1, playerY); break;
            case RIGHT_CHAR: handleMove(playerX, playerY+1); break;
            case LEFT_CHAR: handleMove(playerX, playerY-1); break;
            
        }       
        printField ();
        }while (step != EXIT_CHAR && game);        
        
        
        
    }
    
    public static void game (){
        printField ();
        
        do{move ();}
        while(game);
        
        victory ();
        
        
    }
    
    public static boolean checkStep(int stepX, int stepY) {

        boolean WrongStep = true;

        if (stepX < 0 || stepX >= FIELD_SIZE
                || stepY < 0 || stepY >= FIELD_SIZE) {
            WrongStep = false;
        }
        return WrongStep;
    }
    
    public static void handleMove(int newPlayerX, int newPlayerY) {
        
        checkGame(newPlayerX, newPlayerY);
        
        field [playerX][playerY] = EMPTY;
        
        if(checkStep(newPlayerX,newPlayerY)){
        playerX = newPlayerX;
        playerY = newPlayerY;}       
        
        
        field [playerX][playerY] = PLAYER;       
        
    }
    
    public static void checkGame(int newPlayerX, int newPlayerY){
        
        if (field[newPlayerX][newPlayerY] == GATE) {
            win = true; 
        }
        if (field[newPlayerX][newPlayerY] == MINE) {
            loose = true;
        }
        if (win || loose) {
            game = false;            
        }
        
    }
    
    public static void victory (){
        
        if (win) {
            System.out.println("Győzelem!");
        }
        if (loose) {
            System.out.println("Akna, game over");
        }
        
    }
            
    
}
