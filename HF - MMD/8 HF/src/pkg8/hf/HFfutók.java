package pkg8.hf;
import java.util.Scanner;
public class HFfutók {
    
    public static int numberOfRunners;
    
    public static void main(String[]args){
        
        askNumberOfRunners();
        String [][] namesOfRunners = namesOfRunners();
        Double[] timeOfRunners = askTimeOfRunners();
        
        String [][] namesInOrder = namesOfRunners;
        Double[] timeinOrder = timeOfRunners;
                
        raceInOrder(namesOfRunners,timeOfRunners);
        
        printWinners(namesOfRunners,timeOfRunners);
        
    }
    
    public static void askNumberOfRunners(){
        
        Scanner sc = new Scanner(System.in);
        
        do{
        System.out.println("Adja meg a versenyzők számát");
        numberOfRunners = sc.nextInt();
        if(numberOfRunners < 3){
            System.out.println("Háromnál nagyobb számot adjon meg");
        }
        }while(numberOfRunners < 3);
    }
    
    public static String [][] namesOfRunners(){
        String [][] namesOfRunners = new String [numberOfRunners][2];
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < numberOfRunners; i++){
            System.out.println("Adja meg a(z)"+(i+1)+". futó családnevét");
            namesOfRunners [i][0] = sc.nextLine();
            System.out.println("Adja meg a(z)"+(i+1)+". futó keresztnevét");
            namesOfRunners [i][1] = sc.nextLine();
        }
        
        return namesOfRunners;
    }
    
       public static Double [] askTimeOfRunners(){
        Double[] timesOfRunners = new Double [numberOfRunners];
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < numberOfRunners; i++){
            System.out.println("Adja meg a(z)"+(i+1)+". futó idejét");
            timesOfRunners [i] = sc.nextDouble();
        }
        
        return timesOfRunners;
    }
       
       public static void printWinners(String [][] namesOfRunners,
               Double [] timeOfRunners){
           
           System.out.println("Az első helyezett:");
           System.out.println(namesOfRunners[0][0] + " " + namesOfRunners[0][1]);
           System.out.println("Ideje:" + timeOfRunners[0]);
           
           System.out.println("A második helyezett:");
           System.out.println(namesOfRunners[1][0] + " " + namesOfRunners[1][1]);
           System.out.println("Ideje:" + timeOfRunners[1]);

           System.out.println("A harmadik helyezett:");
           System.out.println(namesOfRunners[2][0] + " " + namesOfRunners[2][0]);
           System.out.println("Ideje:" + timeOfRunners[2]);
       }
       
    public static void raceInOrder(String [][] namesOfRunners,
               Double [] timeOfRunners) {

        for (int i = 0; i < timeOfRunners.length; i++) {
            for (int j = timeOfRunners.length-1; j > i; j--) {
                if(timeOfRunners[i]>timeOfRunners[j]) {
                    double tmp = timeOfRunners[i];
                    timeOfRunners[i] = timeOfRunners[j];
                    timeOfRunners[j] = tmp;
                    
                    String tmpString = namesOfRunners[i][0];
                    namesOfRunners[i][0] = namesOfRunners[j][0];
                    namesOfRunners[j][0] = tmpString;
                    
                    String tmpString2 = namesOfRunners[i][1];
                    namesOfRunners[i][1] = namesOfRunners[j][1];
                    namesOfRunners[j][1] = tmpString;
                    
                }
            }     
        }
 
    }
       
       
    
}
