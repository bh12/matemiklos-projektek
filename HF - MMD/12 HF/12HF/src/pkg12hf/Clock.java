package pkg12hf;
import java.util.Scanner;
public class Clock {
    
    private int hour;
    private int minute;
    private int second;
    
    
    public void printTime(){
        
        handleTime();
        
        System.out.println("Az eltárolt idő: "+hour+" óra "+minute+" perc "+
                second+" másodperc");
    }

    public void setSecond(int plus){
        
        this.second += plus;
        
    }
    
    public void setMinute(int plus){
        
        this.minute += plus;
        
    }
    
        public void setHour(int plus){
        
        this.hour += plus;
        
    }
    
        public void handleTime(){
        
            if (second >= 60) {
                minute += second / 60;
                second = (second % 60);}

            if (minute >= 60) {
                hour += minute / 60;
                minute = (minute % 60);}

            if (hour >= 24) {
                hour -= (hour / 24) * 24;}
    
    }

}