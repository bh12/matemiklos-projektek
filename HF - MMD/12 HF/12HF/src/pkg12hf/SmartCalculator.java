package pkg12hf;

import java.util.Scanner;

public class SmartCalculator {
    
    
    public static Scanner sc = new Scanner(System.in);

    private int number;

    private char operation;//+,-
    private double result;

    //nem kell konstruktor
    
    public void start() {
        System.out.println("Kérem addjon meg egy számot: ");
        this.result = sc.nextDouble();
        askOperation();
    }
    
    
    public void askNumber() {
        System.out.println("Kérem addjon meg egy számot: ");
        this.number = sc.nextInt();
        askOperation();
    }

    public void askOperation() {
        System.out.println("Kérem addjon meg egy műveletet (+,-,=): ");
        char op = sc.next().charAt(0);

        switch (op) {
            case '=':
                //handleEqualOperation();
                printResult();
                break;
            case '+':
                //this.operation = op;
                this.result = result + number;
                askNumber();
                break;
                
            case '-':
                //this.operation = op;
                this.result -= number;
                askNumber();
                break;
        }        
    }

    private void handleEqualOperation() {
        switch (this.operation) {
            case '+':
                result += number;
                break;
            case '-':
                result -= number;
                break;
        }
    }

    public void printResult() {
        System.out.println("result: " + this.result);
    }


    
    
}
