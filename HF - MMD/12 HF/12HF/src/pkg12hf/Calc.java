package pkg12hf;
import java.util.Scanner;
public class Calc {
    
    public static Scanner sc = new Scanner(System.in);

    private int number;

    private char operation;//+,-
    private double result;
    
    
    public void start() {

        System.out.println("Kérem addjon meg egy számot: ");
        this.result = sc.nextInt();
        //System.out.println(result);
        askOperation();
    }

    public void askOperation() {
        System.out.println("Kérem addjon meg egy műveletet (+,-,=): ");
        char op = sc.next().charAt(0);

        switch (op) {
            case '=':                
                printResult();
                break;
            case '+':
                askNumber();
                this.result = result + number;
                askOperation();
                break;

            case '-':
                askNumber();
                this.result -= number;
                askOperation();
                break;
        }
    }

    public void askNumber() {
        System.out.println("Kérem addjon meg egy számot: ");
        this.number = sc.nextInt();    
    }

    public void printResult() {
        System.out.println("result: " + this.result);
    }
    
}
