package pkg6;
import java.util.Scanner;
public class lottó {


    public static void main(String[] args) {
        
        int typeOfLottery = 0;
        Scanner sc = new Scanner(System.in);
        int max = 0;
        
        System.out.println("Adja meg milyen lottót szeretne játszani");
        System.out.println("Hány számmal:");
        typeOfLottery = sc.nextInt();
        System.out.println("Hány számból:");
        do{
        max = sc.nextInt();
        if (typeOfLottery > max){
        System.out.println("A szám legyen kisebb mint: " + typeOfLottery);    
        }
        }while (typeOfLottery > max);
        
        int [] userNumbers = new int [typeOfLottery];
        
        userNumbers = giveNumbers(typeOfLottery);
        
        System.out.println("Találatok száma: " + lotto(userNumbers, max));
        

    }
    
    public static int lotto(int [] userNumbers, int max) {
        
        int [] lotteryNumbers = new int [userNumbers.length];
        boolean same = false;
        
        for (int i = 0; i < userNumbers.length; i++){
            
            do {lotteryNumbers [i] = (int)(Math.random()*(max+1));
            for (int x = 0; x < userNumbers.length; x++){
                if (lotteryNumbers [i] == lotteryNumbers [x])
                    same = true;
            }
            
            
            } while (same);
            same = false;
        }
        
        int matches = 0;
        
        for (int i = 0; i < userNumbers.length; i++){
            for (int x = 0; x < userNumbers.length; x++){
                if (lotteryNumbers [i] == userNumbers [x])
                    matches++;
            }
        }
        
        return matches;
        
    }
    
    public static int [] giveNumbers(int typeOflottery) {
    
    int [] userNumbers = new int [typeOflottery];
    Scanner sc = new Scanner(System.in);
    int x = 1;
    
    for (int i = 0; i < userNumbers.length; i++){
        x = 1 + i;
        System.out.println("Adja meg a(z) "+x+ ". számot");
        
        userNumbers [i] = sc.nextInt();
        
    }
        
    return userNumbers; 
    
    }
    
}
