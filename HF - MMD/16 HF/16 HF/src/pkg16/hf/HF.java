package pkg16.hf;

import java.util.Random;

public class HF {

public static final Random rnd = new Random();
public static int COUNTER;
        
    public static void main(String[] args) {
        Creature[] creatures = new Creature[100];
        generateCreatures(creatures);

        while (true) {
            
            Wolf attackerWolf = selectWolf(creatures);
            Animal poorAnimal = selectPoorAnimal(creatures);

            if (attackerWolf == null || poorAnimal == null) {
                fightCounter();
                break;
            }

            System.out.println(poorAnimal);
            
            boolean bothAlive = true;
            while (bothAlive) {
                if (attackerWolf.isLive()) {
                    attackerWolf.attack();
                    attack(attackerWolf, poorAnimal);
                }

                if (poorAnimal.isLive()) {

                    attack(poorAnimal, attackerWolf);
                }

                bothAlive = attackerWolf.isLive() && poorAnimal.isLive();
            }

            Dog.underAttack = false;
            COUNTER++;

        }
    }
    

    public static void generateCreatures(Creature[] creatures) {
        for (int i = 0; i < creatures.length; i++) {
            int rndNumber = rnd.nextInt(100);
            if (rndNumber < 60) {
                creatures[i] = new Chicken(rnd.nextInt(5));
            } else if (rndNumber < 65) {
                creatures[i] = new Turkey(rnd.nextInt(5));
            } else if (rndNumber < 70) {
                creatures[i] = new Cat(rnd.nextInt(10));
            } else if (rndNumber < 80) {
                creatures[i] = new Dog(rnd.nextInt(20));
            } else {
                creatures[i] = new Wolf(rnd.nextInt(20));
            }
        }

    }
    
    //farkasvalaszto metodus
    public static Wolf selectWolf(Creature[] creatures) {
              Wolf attackerWolf = null;      
        for (int i = 0; i < creatures.length; i++) {
                if (creatures[i] instanceof Wolf && creatures[i].isLive()) {
                    attackerWolf = (Wolf) creatures[i];
                    break;
                }
            }
        
        return attackerWolf;
    }
    
        public static Animal selectPoorAnimal(Creature[] creatures) {
            Animal poorAnimal = null;
            for (int i = 0; i < creatures.length; i++) {
                if (!(creatures[i] instanceof Wolf) && creatures[i].isLive()) {
                    poorAnimal = (Animal) creatures[i];
                    break;
                }
            }
        
        return poorAnimal;
    }
        
        public static void fightCounter(){
                System.out.println("counter: " + COUNTER);            
        }
        
        public static void attack(Animal attacker, Animal poorAnimal){
            
            int attackScore = 0;
            
            if (attacker instanceof AttackInterface) {
                attackScore = ((AttackInterface) attacker).getAttackScore();
            }
            
            poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackScore);
            
        }
}
        
        
        
    
    

