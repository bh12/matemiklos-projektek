package pkg30.hf.swing.game;

import javax.swing.JPanel;

public class SwingController {

    private SwingView view;
    private SwingModel model;

    public SwingController() {
        this.view = new SwingView(this);
        this.view.init();
        this.model = new SwingModel();
    }

    public void handleButtonClick() {

        int buttonIndex2 = 0;

        do {buttonIndex2 = (int) (Math.random() * (8));
        } while (model.getNumber() == buttonIndex2);

        model.setNumber(buttonIndex2);
        updateView();

    }

    public void updateView() {

        view.getPanel().removeAll();
        view.setUpbuttons(model.getNumber());
        view.repaint();
    }

    public SwingModel getModel() {
        return model;
    }

}
