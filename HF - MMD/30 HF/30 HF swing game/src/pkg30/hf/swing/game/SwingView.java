package pkg30.hf.swing.game;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SwingView extends JFrame implements ActionListener {

    private SwingController controller;

    private List<JButton> buttons = new ArrayList<>();
    private JPanel panel;

    public SwingView(SwingController controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        createButtons();
        setUpbuttons(10);
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(400, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("Házi - Swing Játék");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setSize(300, 400);
        panel.setLayout(new GridLayout(3, 3));

        add(panel);

    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void createButtons() {

        for (int i = 0; i < 9; i++) {

            JButton button = new JButton(String.valueOf(i));
            buttons.add(button);
        }
    }

    protected void setUpbuttons(int number) {

        for (int i = 0; i < buttons.size(); i++) {

            if (i != number) {
                buttons.get(i).addActionListener(this);
                this.panel.add(buttons.get(i));
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        controller.handleButtonClick();
        
    }

    public List<JButton> getButtons() {
        return buttons;
    }

    public void setButtons(List<JButton> buttons) {
        this.buttons = buttons;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }
    
}
