package pkg30.hf.swing.game;

import java.util.*;
import javax.swing.JButton;

public class SwingModel {

    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
}
