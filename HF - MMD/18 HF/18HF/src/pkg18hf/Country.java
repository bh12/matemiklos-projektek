package pkg18hf;
import java.util.*;
public class Country implements Comparable<Country>{
    
    private String name;
    private float populationMillions;
    private List <City> cities = new ArrayList<>();

    public Country(String name, List cities) {
        this.name = name;
        this.cities = cities;
        setpopulationMillions(cities);
    }
    
    public Country(String name, City city) {
        this.name = name;
        this.cities.add(city);
        setpopulationMillions(this.cities);
    }

    public Country(String name) {
        this.name = name;
    }

    public void setpopulationMillions(List <City> cities){
        
        float sum = 0;
        
        for (City c: cities){
            sum += c.getPopulationThousands();
        }
        
        sum = sum / 1000;
        this.populationMillions = sum;
    }
    
        @Override
    public int compareTo(Country o){
        
        if(this.populationMillions < o.getPopulationMillions()){
            return -1;
        }
        else if(this.populationMillions == o.getPopulationMillions()){
            return 0;
        }else{
            return 1;
        }
    }

    public float getPopulationMillions() {
        return populationMillions;
    }
    
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
        setpopulationMillions(this.cities);
    }
    
    public void setCities(City city) {
        this.cities.add(city);
        setpopulationMillions(this.cities);
    }

    @Override
    public int hashCode() {
        
        return this.cities.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (!Objects.equals(this.cities, other.cities) && 
                this.cities.size() != other.cities.size()) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return name;
    }   
    
}
