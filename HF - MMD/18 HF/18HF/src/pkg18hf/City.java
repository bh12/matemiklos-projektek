package pkg18hf;
import java.util.*;
public class City {
    
    private String name;
    private float populationThousands;

    public City(String name, float populationThousands) {
        this.name = name;
        this.populationThousands = populationThousands;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPopulationThousands() {
        return populationThousands;
    }

    public void setPopulationThousands(float populationThousands) {
        this.populationThousands = populationThousands;
    }

    @Override
    public int hashCode() {
        int hash;
        hash = Objects.hashCode(this.name);
        hash = hash + Float.floatToIntBits(this.populationThousands);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final City other = (City) obj;
        if (this.populationThousands != other.populationThousands) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "(" + name + ", " + populationThousands + ")";
    }
    
    
    
}
