package pkg18hf;
import java.util.*;
public class Main {

    public static void main(String[] args) {
        
        City bp = new City("Budapest", 2000);
        City sze = new City("Szentendre", 26.4f);
        City vac = new City("Vác", 32.8f);
        City deb = new City("Debrecen", 201.4f);
        City vis = new City("Visegrád", 1.8f);
        
        City fct = new City("First City", 4500);
        City qnl = new City("qin'Lat", 1700);
        City mol = new City("Molor", 32.7f);
                        
        List <City> hunCities = new ArrayList();
        List <City> hun2Cities = new ArrayList();
        List <City> klinCities = new ArrayList();
        
        hunCities.add(bp);
        hunCities.add(sze);
        hunCities.add(vac);
        hunCities.add(deb);
        hunCities.add(vis);
        
        hun2Cities.add(bp);
        hun2Cities.add(sze);
        hun2Cities.add(vac);
        hun2Cities.add(deb);
        hun2Cities.add(vis);
        
        klinCities.add(fct);
        klinCities.add(qnl);
        klinCities.add(mol);
        
        
        Country hun = new Country("Hungary", hunCities);
        Country hun2 = new Country("Hungary2", hun2Cities);
        Country klin = new Country("Klingon Empire", klinCities);
        
        
        Set<Country> countriesHashSet = new HashSet<>();
        Set<Country> countriesTreeSet = new TreeSet<>();
        
        hun2.setCities(fct);
        
        countriesHashSet.add(hun);
        countriesHashSet.add(hun2);
        countriesHashSet.add(klin);
        
        countriesTreeSet.add(hun);
        countriesTreeSet.add(hun2);
        countriesTreeSet.add(klin);
        
        //hun2.setCities(fct);
        
        System.out.println(countriesHashSet.size());
        System.out.println(hunCities);
        System.out.println(hun2.getCities());
        System.out.println(klin.getPopulationMillions());
        System.out.println(countriesTreeSet);
        countriesTreeSet.remove(hun);
        System.out.println(countriesTreeSet);
        
        hun2.setCities(hunCities);
        countriesHashSet.clear();
        countriesHashSet.addAll(countriesTreeSet);
        System.out.println(countriesHashSet.size());
    }
    
}
