
package pkg28.hf;

public enum Weekdays {
    HETFO("Hétfő"),
    KEDD("Kedd"),
    SZERDA("Szerda"),
    CSUTORTOK("Csütörtök"),
    PENTEK("Péntek"),
    SZOMBAT("Szombat"),
    VASARNAP("Vasárnap");
    
    String message;
    
    private Weekdays(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    
}
