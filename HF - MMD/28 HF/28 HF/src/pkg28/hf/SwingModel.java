package pkg28.hf;

import java.util.*;

public class SwingModel {

    private List<Log> logs;

    public SwingModel() {
        this.logs = new ArrayList<>();
    }

    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }


}
