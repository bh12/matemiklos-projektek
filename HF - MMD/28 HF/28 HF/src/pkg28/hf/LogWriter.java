package pkg28.hf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class LogWriter {

    private List<Log> logs;

    public LogWriter(List<Log> logs) {
        this.logs = logs;
    }

    public void writeLogsToText() {

        File f = new File("SwingLogs.txt");

        try (FileWriter fw = new FileWriter(f);
                BufferedWriter bw = new BufferedWriter(fw);) {

            String line = null;
            StringBuilder stb = new StringBuilder();

            int hour1 = logs.get(0).getHour();

            for (int i = 0; i < logs.size(); i++) {

                int hour2 = logs.get(i).getHour();

                if (hour1 == hour2) {
                    line = logs.get(i).getTextToPrint();
                    stb.append(line)
                            .append(System.lineSeparator());
                } else {
                    line = logs.get(i).getTextToPrint();
                    stb.append(line)
                            .append(System.lineSeparator())
                            .append(System.lineSeparator());
                }

                hour1 = logs.get(i).getHour();
            }

            line = stb.toString();
            fw.write(line);

            bw.close();

        } catch (IOException e) {
            System.out.println("Hiba történt a fájlírás részben");
        }

    }

}
