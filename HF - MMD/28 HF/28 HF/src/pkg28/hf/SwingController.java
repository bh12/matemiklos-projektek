package pkg28.hf;

import java.time.LocalDateTime;
import java.util.Date;

public class SwingController {

    private SwingView view;
    private SwingModel model;

    public SwingController() {
        this.view = new SwingView(this);
        this.view.init();
        this.model = new SwingModel();
    }

    public void handleButtonClick() {

        updateView();

    }

    public void updateView() {

        Log l1 = new Log();

        view.setScreenText(l1.getTextToPrint() + System.lineSeparator());

        model.getLogs().add(l1);

        LogWriter lw = new LogWriter(model.getLogs());
        lw.writeLogsToText();

    }

    public SwingModel getModel() {
        return model;
    }

}
