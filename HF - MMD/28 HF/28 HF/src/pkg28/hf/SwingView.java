package pkg28.hf;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SwingView extends JFrame {

    private SwingController controller;
    
    private JButton button1;
    private JTextArea text1;
    private JPanel panel;
    private GridLayout gl;

    public SwingView(SwingController controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpTextField();
        setUpbutton1();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(400, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("ButtonAndLogSwing");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setSize(300, 400);
        panel.setLayout(new GridLayout(2, 1));

        add(panel);
    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpTextField() {
        this.text1 = new JTextArea();
        Font f = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
        this.text1.setFont(f);
        panel.add(this.text1);
    }
    
    private void setUpbutton1() {
        this.button1 = new JButton("PushTheButton");
        this.button1.addActionListener(event -> {controller.handleButtonClick();});       
        panel.add(this.button1);
    }
    
        public void setScreenText(String screenText) {
        this.text1.append(screenText);
    } 
}
