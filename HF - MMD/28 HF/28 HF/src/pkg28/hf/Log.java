package pkg28.hf;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Date;

public class Log implements Comparator<Log>{
    
    private final String displayedText = "Button pressed";
    private LocalDateTime currentDate;
    private static int counter;
    private int hour;
    
    private String textToPrint;

    public Log() {
        this.currentDate = LocalDateTime.now();
        this.hour = currentDate.getHour();
        
        this.textToPrint =
                String.valueOf(counter+1)
                + ". "
                + String.valueOf(currentDate)
                + "; "
                + displayedText;
        
        counter++;
    }

    public String getDisplayedText() {
        return displayedText;
    }

    public LocalDateTime getCurrentDate() {
        return currentDate;
    }

    public static int getCounter() {
        return counter;
    }

    public int getHour() {
        return hour;
    }

    public String getTextToPrint() {
        return textToPrint;
    }
        
    @Override
    public int compare(Log a, Log b) 
    { 
        return a.getHour() - b.getHour(); 
    } 
    
}
