package pkg6.hf;

import java.util.Scanner;

public class HF5 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int userInput = 0;

        do {
            System.out.println("Adja meg a fibonacci sorozat elemét");
            userInput = sc.nextInt();
        } while (userInput < 0);

        System.out.println(fibonacciFor(userInput));
        System.out.println(fibonacciRec(userInput));

    }
    
    public static int fibonacciFor(int a) {

        int[] fib = new int[a + 1];
        fib[0] = 0;
        if (a > 0) {
            fib[1] = 1;
        }

        if (a > 1) {
            for (int i = 2; i <= a; i++) {

                fib[i] = fib[i - 2] + fib[i - 1];

            }
        }

        return fib[a];
        
}
    public static int fibonacciRec(int a) {

        if ((a == 0) || (a == 1)) {
            return a;
        } else {
            return fibonacciRec(a - 1) + fibonacciRec(a - 2);
        }

    }
    
}
