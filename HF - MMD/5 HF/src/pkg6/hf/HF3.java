package pkg6.hf;

public class HF3 {
    public static void main(String[] args) {
        
        int arrayDimension = (int) (Math.random()*11+10);
        int [] array1 = new int [arrayDimension];
        int [] array2 = new int [arrayDimension];
        
        fillArray(array1);
        fillArray(array2);
        
        System.out.println(sumArray(array1)+sumArray(array2));
        
    }
    
    public static void fillArray(int [] array) {
        
            for (int i = 3; i < array.length; i++) {
            array[i] = (int) (Math.random() * 90 + 1);
            }
    }
    
    public static int sumArray(int[] array) {
        
            int sumArray = 0;
            
            for (int i = 0; i < array.length; i++) {
                sumArray = sumArray + array[i];
            }
            
            return sumArray;

    } 

}
