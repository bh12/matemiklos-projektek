package pkg6.hf;
import java.util.Scanner;
public class HF4 {
    
    public static void main(String[] args) {

        int userChoose = 0;
        do {
            System.out.print("Kérem válasszon:" + System.lineSeparator()
                    + "1: Üdvözlés" + System.lineSeparator()
                    + "2: hatoslottó számok" + System.lineSeparator()
                    + "3: ötöslottó számok" + System.lineSeparator()
                    + "4: kilépés" + System.lineSeparator());

            Scanner sc = new Scanner(System.in);
            userChoose = sc.nextInt();

            switch (userChoose) {
                case 1:
                    hello();
                    break;
                case 2:
                    lottery6();
                    break;
                case 3:
                    lottery5();
                    break;

            }
        } while (userChoose != 4);
    }
    
    public static void hello (){
        
        System.out.println("Adja meg a nevét");
        
        Scanner sc = new Scanner(System.in);
        String name = sc.next();
        
        System.out.println("Hello " + name);
        
    }
    
    public static void lottery6 (){
        
        int number = 0;
        int previousNumber = 0;
        
        System.out.println("Az ön hatoslottó számai: ");
        
            int randNumber1 = 0;
            int randNumber2 = 0;        
        
        for (int i = 0; i < 6; i++){
            
            if (randNumber1 == randNumber2){
                randNumber1 = (int)(Math.random()*(45+1));
            }
            
            System.out.print(randNumber1);
            if (i < 5) {System.out.print(", ");}
            randNumber1 = randNumber2;
        }
            System.out.print(System.lineSeparator());
        }
    
       public static void lottery5 (){
        
        int number = 0;
        int previousNumber = 0;
        
        System.out.println("Az ön ötöslottó számai: ");
        
            int randNumber1 = 0;
            int randNumber2 = 0;        
        
        for (int i = 0; i < 5; i++){
            
            if (randNumber1 == randNumber2){
                randNumber1 = (int)(Math.random()*(90+1));
            }
            
            System.out.print(randNumber1);
            if (i < 4) {System.out.print(", ");}
            randNumber1 = randNumber2;
        }
            System.out.print(System.lineSeparator());
        }
}
