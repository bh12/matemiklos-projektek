package pkg6.hf;

public class HF1 {

    public static void main(String[] args) {
        int[] array = new int[20];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 90 + 10);
        }
        
        listArray(array);
        evenSumArray(array);
        ifDivByFiveArray(array);
        firstOddNumInArray(array);
        is32InArray(array);
        
    }

        public static void listArray(int[] array) {

        System.out.println("A tömb elemei:");

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
            if (i < (array.length - 1)) {
                System.out.print(", ");
            }
        }
        System.out.print(System.lineSeparator());

    }
    
        public static void evenSumArray(int[] array) {

        System.out.println("A tömb páros elemeinek összege:");
        
        int sumOfEvenNumbers = 0;

        for (int i = 0; i < array.length; i++) {
            if (array [i] % 2 == 0) {
                sumOfEvenNumbers = sumOfEvenNumbers + array [i];
            }
        }
            System.out.println(sumOfEvenNumbers);
        

    }
        
        public static void ifDivByFiveArray(int[] array) {
       
        boolean ifDivByFive = false;

        for (int i = 0; i < array.length; i++) {
            if (array [i] % 5 == 0) {
                ifDivByFive = true;
            }
        }
            
        if (ifDivByFive){
        System.out.println("Van öttel osztható szám a tömben");}
        else {
        System.out.println("Nincs öttel osztható szám a tömben");}
        

    }  
    
        public static void firstOddNumInArray(int[] array) {

        System.out.println("Az első páratlan szám a tömben:");
        
        int firstOddNumber = 0;

        for (int i = array.length-1; i >= 0; i--) {
            if (array [i] % 2 != 0) {
                firstOddNumber = array [i];
            }
        }
            System.out.println(firstOddNumber);
        

    }
                
        public static void is32InArray(int[] array) {


        boolean isIt32 = false;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 32) {
                isIt32 = true;
            }
        }
        if (isIt32){
        System.out.println("Van 32 a tömben");}
        else {
        System.out.println("Nincs 32 a tömben");}

    }
    
}
