package SwingGyakorlás;

import java.io.Serializable;

public class Emplyee implements Serializable{

    private String text;

    public Emplyee() {
        this.text = "Employee";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
