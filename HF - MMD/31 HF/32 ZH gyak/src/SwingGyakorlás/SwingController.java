package SwingGyakorlás;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

public class SwingController {

    private SwingView view;
    private SwingModel model;
    private JFileChooser jfc;
    FileNameExtensionFilter restrict;

    public SwingController() {
        this.view = new SwingView(this);
        this.view.init();
        this.model = new SwingModel();
        this.jfc = new JFileChooser(FileSystemView.getFileSystemView());
    }

    public void setupFileChooser() {
        jfc.setAcceptAllFileFilterUsed(false);
        restrict = new FileNameExtensionFilter("Only .ser files", ".ser");
        jfc.addChoosableFileFilter(restrict);
    }

    public void handleBossButtonClick() {
        Boss b = new Boss();
        model.getEmplyeeList().add(b);
    }

    public void handleWorkerButtonClick() {
        Worker w = new Worker();
        model.getEmplyeeList().add(w);
    }

    public void handleSerSaveButtonClick() {
        serSave();
    }

    public void handleSerLoadButtonClick() {
        int a = jfc.showOpenDialog(null);

        if (a == JFileChooser.APPROVE_OPTION) {
            File f = (jfc.getSelectedFile());

            serLoad(f);

        } else {
            view.getText1().setText("Rossz fájltípust választott");
            view.repaint();
        }
    }

    public void serSave() {
        try (FileOutputStream fos = new FileOutputStream("EmplyeeSer.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            List<Emplyee> EmplyeeList = model.getEmplyeeList();
            oos.writeObject(EmplyeeList);

        } catch (FileNotFoundException ex) {
            System.out.println(".ser létrehozás hiba");
        } catch (IOException ex) {
            System.out.println("IO hiba létrehozáskor");
        }
    }

    public void serLoad(File f) {
        try (FileInputStream fis = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fis)) {

            try {
                List<Emplyee> EmplyeeList = (List) ois.readObject();
                model.setEmplyeeList(EmplyeeList);

            } catch (ClassNotFoundException ce) {
                System.out.println("ClassNotFound hiba");
            }

        } catch (FileNotFoundException ex) {
            System.out.println(".ser not found hiba");
        } catch (IOException ex) {
            System.out.println("IO hiba serLoadnál");
        }
    }

    public void logOnTextArea(String s) {
        String logMessage;
        LocalDateTime ld = LocalDateTime.now();

        logMessage = ld.toString() + "object type: " + s;

        view.getText1().setText(logMessage);
        view.repaint();
    }

    public void ControllSout() {

        for (int i = 0; i < model.getEmplyeeList().size(); i++) {
            System.out.println(model.getEmplyeeList().get(i).getText());
        }

    }

    public SwingModel getModel() {
        return model;
    }

    public void updateView() {

        LocalDate ld = LocalDate.now();
        
        view.setScreenText(ld + System.lineSeparator());

    }

}
