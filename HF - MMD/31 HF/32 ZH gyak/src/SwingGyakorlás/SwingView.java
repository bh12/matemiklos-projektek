package SwingGyakorlás;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class SwingView extends JFrame implements ActionListener {

    private final SwingController controller;

    private JPanel panel;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JTextArea text1;

    public SwingView(SwingController controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpTextArea();
        setUpbutton();
        buttonActions();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(400, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("Swing ZH gyak");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setSize(400, 500);
        panel.setLayout(new GridLayout(0, 1));

        add(panel);

    }

    private void showWindow() {
        this.setVisible(true);
    }

    protected void setUpbutton() {

        this.button1 = new JButton("Worker létrehozása");
        this.panel.add(button1);

        this.button2 = new JButton("Boss létrehozása");
        this.panel.add(button2);

        this.button3 = new JButton("Serializálás");
        this.panel.add(button3);

        this.button4 = new JButton("Beolvasás");
        this.panel.add(button4);

    }

    protected void setUpTextArea() {

        this.text1 = new JTextArea(1, 1);
        this.panel.add(text1);

    }

    public void buttonActions() {

        button1.addActionListener((ActionEvent e) -> {
            controller.handleWorkerButtonClick();
            controller.ControllSout();
            controller.updateView();
        });

        button2.addActionListener((ActionEvent e) -> {
            controller.handleBossButtonClick();
            controller.ControllSout();
            controller.updateView();
        });

        button3.addActionListener((ActionEvent e) -> {
            controller.handleSerSaveButtonClick();
            controller.ControllSout();
            controller.updateView();
        });

        button4.addActionListener((ActionEvent e) -> {
            controller.handleSerLoadButtonClick();
            controller.ControllSout();
            controller.updateView();
        });

    }

    public JTextArea getText1() {
        return text1;
    }

    public void setText1(JTextArea text1) {
        this.text1 = text1;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public void setScreenText(String screenText) {
        this.text1.append(screenText);
    }
}
