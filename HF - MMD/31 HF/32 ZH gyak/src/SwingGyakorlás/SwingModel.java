package SwingGyakorlás;

import java.util.*;

public class SwingModel {

    private List<Emplyee> EmplyeeList;

    public SwingModel() {
        this.EmplyeeList = new ArrayList<>();
    }

    public List<Emplyee> getEmplyeeList() {
        return EmplyeeList;
    }

    public void setEmplyeeList(List<Emplyee> EmplyeeList) {
        this.EmplyeeList = EmplyeeList;
    }

}
