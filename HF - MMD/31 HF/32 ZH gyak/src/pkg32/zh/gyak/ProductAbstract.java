package pkg32.zh.gyak;

import java.time.LocalDate;
import java.util.List;

public abstract class ProductAbstract {
    
    private ProductTypes type;
    private String manufacturer; 
    private int code;
    private int price;
    private LocalDate dateOfRegistry;

    public ProductAbstract(ProductTypes type, String manufacturer, int code, int price) {
        this.type = type;
        this.manufacturer = manufacturer;
        this.code = code;
        this.price = price;
        this.dateOfRegistry = LocalDate.now();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.code;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductAbstract other = (ProductAbstract) obj;
        if (this.code != other.code) {
            return false;
        }
        return true;
    }

    public ProductTypes getType() {
        return type;
    }

    public void setType(ProductTypes type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getDateOfRegistry() {
        return dateOfRegistry;
    }

    public void setDateOfRegistry(LocalDate dateOfRegistry) {
        this.dateOfRegistry = dateOfRegistry;
    }

}
