package pkg32.zh.gyak;

import java.time.LocalDate;

public class KitchenProduct extends ProductAbstract{
    
    private final boolean canBeLift = true;
    private final boolean canSwitchOn = true;

    public KitchenProduct(ProductTypes type, String manufacturer, int code, int price) {
        super(type, manufacturer, code, price);
    }
 
    
}
