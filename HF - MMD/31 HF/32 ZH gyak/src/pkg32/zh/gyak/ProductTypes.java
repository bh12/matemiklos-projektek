package pkg32.zh.gyak;

public enum ProductTypes {
    CHEAP,
    AVERAGE,
    EXPENSIVE,
    LUXURY,
    POPULAR;
    
    private String meassage;
    
    public static ProductTypes getCHEAP() {    
        return CHEAP;
    }

    public static ProductTypes getAVERAGE() {
        return AVERAGE;
    }

    public static ProductTypes getEXPENSIVE() {
        return EXPENSIVE;
    }

    public static ProductTypes getLUXURY() {
        return LUXURY;
    }

    public static ProductTypes getPOPULAR() {
        return POPULAR;
    }
    
        public static ProductTypes consolChooser(int i) {

        ProductTypes pt;
            
        switch (i) {
            case 0:
                pt = ProductTypes.CHEAP;
                break;
            case 1:
                pt = ProductTypes.AVERAGE;
                break;
            case 2:
                pt = ProductTypes.EXPENSIVE;
                break;
            case 3:
                pt = ProductTypes.LUXURY;
                break;
            case 4:
                pt = ProductTypes.POPULAR;
                break;
            default:
                pt = ProductTypes.CHEAP;

        }
        
        return pt;
    }
}
