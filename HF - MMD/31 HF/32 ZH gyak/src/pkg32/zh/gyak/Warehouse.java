package pkg32.zh.gyak;

import java.time.Clock;
import java.util.*;

public class Warehouse {

    List<ProductAbstract> stock = new ArrayList<>();

    private int wareHouseID;

    public Warehouse(int wareHouseID) {
        this.wareHouseID = wareHouseID;
    }

    public void ADD(int code, String manufacturer, ProductTypes type, int price, SpecialTypes st) {
        switch (st) {
            case KITCHEN:
                KitchenProduct kp = new KitchenProduct(type, manufacturer, code, price);
                stock.add(kp);
                break;
            case ENTERTAINMENT:
                EntertainmentProduct ep = new EntertainmentProduct(type, manufacturer, code, price);
                stock.add(ep);
                break;
            case COSMETIC:
                CosmeticPruduct cp = new CosmeticPruduct(type, manufacturer, code, price);
                stock.add(cp);
                break;
            default:
                System.out.println("Hibás műkösés az ADD-nál");
        }

    }

    public void REMOVE(int code) {
        KitchenProduct kp = new KitchenProduct(ProductTypes.AVERAGE, "valami", code, 0);

        if (stock.contains(kp)) {
            stock.remove(kp);
        }
    }

    public void REPORT() {

        System.out.println("Number of products in the warehouse: " + stock.size());

        int counter = 0;
        for (int i = 0; i < stock.size(); i++) {

            if (stock.get(i) instanceof EntertainmentProduct) {
                EntertainmentProduct ep = (EntertainmentProduct) stock.get(i);

                if (ep.getSwitchCounter() >= 2) {
                    counter++;
                }
            }
        }

        System.out.println("Number of EntertainmentProduct with more than two"
                + " switch on: " + counter);

    }

    public List<ProductAbstract> getStock() {
        return stock;
    }

    public void setStock(List<ProductAbstract> stock) {
        this.stock = stock;
    }

    public int getWareHouseID() {
        return wareHouseID;
    }

    public void setWareHouseID(int wareHouseID) {
        this.wareHouseID = wareHouseID;
    }

    @Override
    public String toString() {
        return "Warehouse{" + "wareHouseID=" + wareHouseID + '}';
    }

    public void ConsolMenu() {

        String s = "";

        do {
            System.out.println("Consol menu:"
                    + "ADD(int code, String manufacturer, ProductTypes type (0-4),"
                    + " int price, SpecialTypes st (0-2)"
                    + "REMOVE(int code)"
                    + "REPORT()"
                    + "EXIT: x"
            );

            Scanner sc = new Scanner(System.in);
            s = sc.nextLine();

            if(!s.equals("x")){
            String[] code = s.split(" ");

            switch (code[0]) {
                case "ADD":

                    this.ADD(Integer.valueOf(code[1]),
                            code[2],
                            ProductTypes.consolChooser(Integer.valueOf(code[3])),
                            Integer.valueOf(code[4]),
                            SpecialTypes.consolChooser(Integer.valueOf(code[5]))
                            );
                    break;
                case "REMOVE":
                    this.REMOVE(Integer.valueOf(code[1]));
                    break;
                case "REPORT":
                    this.REPORT();
                    break;
                default:
                    System.out.println("Hibás működés a konzol menünél");
            }
            }
        }while (!s.equals("x"));
    }

}
