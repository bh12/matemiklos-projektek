package pkg32.zh.gyak;

import java.time.LocalDate;

/**
 *
 * @author MMD
 */
public class CosmeticPruduct extends ProductAbstract{
    
    private int weight;

    public CosmeticPruduct(ProductTypes type, String manufacturer, int code, int price) {
        super(type, manufacturer, code, price);
        this.weight = 0;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
