package pkg32.zh.gyak;

import java.time.LocalDate;

public class EntertainmentProduct extends ProductAbstract {

    private final boolean canSwitchOn = true;
    private boolean On;
    private int switchCounter;

    public EntertainmentProduct(ProductTypes type, String manufacturer, int code, int price) {
        super(type, manufacturer, code, price);
        this.On = false;
        this.switchCounter = 0;
    }

    public boolean isIsOn() {
        return On;
    }

    public void setIsOn(boolean On) throws EntertainmentProductException {
        this.On = On;

        this.switchCounter++;

        if (this.switchCounter >= 5) {

            EntertainmentProductException epe = new EntertainmentProductException();

            throw epe;
        }
    }

    public int getSwitchCounter() {
        return switchCounter;
    }

}
