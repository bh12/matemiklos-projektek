package pkg32.zh.gyak;

public enum SpecialTypes {
    KITCHEN,
    ENTERTAINMENT,
    COSMETIC;

    public static SpecialTypes getKITCHEN() {
        return KITCHEN;
    }

    public static SpecialTypes getENTERTAINMENT() {
        return ENTERTAINMENT;
    }

    public static SpecialTypes getCOSMETIC() {
        return COSMETIC;
    }

    public static SpecialTypes consolChooser(int i) {

        SpecialTypes st;
        
        switch (i) {
            case 0:
                st = SpecialTypes.KITCHEN;
                break;
            case 1:
                st = SpecialTypes.ENTERTAINMENT;
                break;
            case 2:
                st = SpecialTypes.COSMETIC;
                break;
            default:
                st = SpecialTypes.KITCHEN;
        }
        
        return st;
    }
}
