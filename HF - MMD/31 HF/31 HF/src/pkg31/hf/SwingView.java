package pkg31.hf;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class SwingView extends JFrame implements ActionListener {

    private final SwingController controller;

    private JPanel panel;
    private JButton button1;
    private JButton button2;
    private JTextArea text1;

    public SwingView(SwingController controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpTextArea();
        setUpbutton();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(400, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("Házi - Swing + serial");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setSize(400, 500);
        panel.setLayout(new GridLayout(0, 1));

        add(panel);
        

    }

    private void showWindow() {
        this.setVisible(true);
    }

    protected void setUpbutton() {

        this.button1 = new JButton("Beolvasás");
        this.panel.add(button1);
        
        this.button2 = new JButton("Kiírás");
        this.panel.add(button2);

        buttonActions();
    }

    protected void setUpTextArea() {

        this.text1 = new JTextArea(1,1);
        this.panel.add(text1);

    }


    public void buttonActions() {

        button1.addActionListener((ActionEvent e) -> {
            controller.handleReadButtonClick();
        });
        
        button2.addActionListener((ActionEvent e) -> {
            controller.handleWriteButtonClick();
        });

    }

    public JTextArea getText1() {
        return text1;
    }

    public void setText1(JTextArea text1) {
        this.text1 = text1;
    }

        @Override
    public void actionPerformed(ActionEvent e) {

        }
}
