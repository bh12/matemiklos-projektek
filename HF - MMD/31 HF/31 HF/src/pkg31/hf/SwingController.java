package pkg31.hf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

public class SwingController {

    private SwingView view;
    private SwingModel model;
    private JFileChooser jfc;
    FileNameExtensionFilter restrict;

    public SwingController() {
        this.view = new SwingView(this);
        this.view.init();
        this.model = new SwingModel();
        this.jfc = new JFileChooser(FileSystemView.getFileSystemView());
    }

    public void setupFileChooser() {
        jfc.setAcceptAllFileFilterUsed(false);
        restrict = new FileNameExtensionFilter("Only .txt or .ser files", "txt", ".ser");
        jfc.addChoosableFileFilter(restrict);
    }

    public void handleReadButtonClick() {

        int a = jfc.showOpenDialog(null);
        String extTxt = "txt";
        String extSer = "ser";

        if (a == JFileChooser.APPROVE_OPTION) {
            File f = (jfc.getSelectedFile());
            String extension = getExtension(f);

            if (extension.equals(extTxt)) {
                readFile(f);
            } else if (extension.equals(extSer)) {
                serLoad(f);
            } else {
                view.getText1().setText("Hiba történt a kiterjesztés vizsgálatakor");
                view.repaint();
            }

        } else {
            view.getText1().setText("Rossz fájltípust választott");
            view.repaint();
        }

    }

    public void handleWriteButtonClick() {

        int a = jfc.showSaveDialog(null);

        if (a == JFileChooser.APPROVE_OPTION) {
            File f = jfc.getSelectedFile();

            writeFile(f);

        } else {
            view.getText1().setText("Hiba történt");
            view.repaint();
        }

    }

    public void writeFile(File f) {

        try (FileWriter fw = new FileWriter(f); BufferedWriter bw = new BufferedWriter(fw)) {

            Book book = model.getBook();
            String str = book.getText();
            bw.write(str);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void readFile(File f) {

        try (FileReader fr = new FileReader(f); BufferedReader br = new BufferedReader(fr)) {

            StringBuilder sb = new StringBuilder();

            String st;
            while ((st = br.readLine()) != null) {
                sb.append(st).append("\n");
            };

            st = sb.toString();
            view.getText1().setText(st);
            model.getBook().setText(st);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public SwingModel getModel() {
        return model;
    }

    public String getExtension(File f) {

        String fileName = f.getName();
        String fileExtension = null;

        if (fileName.contains(".") && fileName.lastIndexOf(".") != 0) {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
        }

        return fileExtension;
    }

    public void serLoad(File f) {
        try (FileInputStream fis = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fis)) {

            try {
                Book b1 = (Book) ois.readObject();
                model.setBook(b1);
                
                System.out.println(model.getBook().getText());
                
            } catch (ClassNotFoundException ce) {
                System.out.println("ClassNotFound hiba");
            }

        } catch (FileNotFoundException ex) {
            System.out.println(".ser not found hiba");
        } catch (IOException ex) {
            System.out.println("IO hiba serLoadnál");
        }
    }
}
