package pkg31.hf;

public class SwingModel {

    private Book book;

    public SwingModel() {
        this.book = new Book();
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

}
