package pkg31.hf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class HF {

    public static void main(String[] args) {
        SwingController controller = new SwingController();

        try (FileOutputStream fos = new FileOutputStream("bookSer.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            Book b1 = new Book();
            b1.setText("Serialisable teszt");
            oos.writeObject(b1);

        } catch (FileNotFoundException ex) {
            System.out.println(".ser létrehozás hiba");
        } catch (IOException ex) {
            System.out.println("IO hiba létrehozáskor");
        }
    }

}
