package pkgenum;


public class Person {
    
    private String name;
    private int age;
    private String Currency;

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, int age, int Currency) {
        this.name = name;
        this.age = age;
        this.Currency = Currency;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", Curreny=" + Currency'}';
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCurreny() {
        return curreny;
    }

    public void setCurreny(enum Curreny) {
        this.curreny = curreny;
    }
    
    
}
