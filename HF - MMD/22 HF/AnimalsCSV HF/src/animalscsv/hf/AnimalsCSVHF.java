package animalscsv.hf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnimalsCSVHF {

    public static void main(String[] args) {
        
        List<String> animalsString = AnimalRead.animalReader("animals.csv");        
        
        List<animal> animals = AnimalRead.animalLine(animalsString);

        

        int fur = 0;
        int feather = 0;
        int fourLeg = 0;
        int breath = 0;
        
        Set<Integer> class_type = new HashSet<>();
        
        for(int i = 0; i < animals.size(); i++){
            if(animals.get(i).isHair())
            fur++;
            if(animals.get(i).isFeathers())
            feather++;
            if(animals.get(i).getLegs() == 4)
            fourLeg++;
            if(!animals.get(i).isBreathes())
            breath++;
            
            class_type.add(animals.get(i).getClass_type());
        }
        
        System.out.println("b. feladat: " + fur);
        System.out.println("c. feladat: " + feather);
        System.out.println("d. feladat: " + fourLeg);
        System.out.println("f. feladat: " + breath);
        
        List<Integer> class_typeList = new ArrayList<>();
        class_typeList.addAll(class_type);
        
        System.out.println("e feladat:");
        for (int i = 0; i < class_typeList.size(); i++) {

            int counter = 0;
            for (int j = 0; j < animals.size(); j++) {
                if (animals.get(j).getClass_type() == class_typeList.get(i)) {
                    counter++;
                }
            }
            System.out.println("class_type " + class_typeList.get(i) + ": " + counter);
        }
        
        System.out.println("g feladat:");
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).isBackbone() && animals.get(i).isPredator()) {
                System.out.println(animals.get(i));
            }
        }

                
    }
    
}
