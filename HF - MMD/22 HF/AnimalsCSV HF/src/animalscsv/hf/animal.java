package animalscsv.hf;

public class animal {
    
    private String name;

    private boolean hair;
    private boolean feathers;
    private boolean eggs;
    private boolean milk;
    private boolean airborne;
    private boolean aquatic;
    private boolean predator;
    private boolean toothed;
    private boolean backbone;
    private boolean breathes;
    private boolean venomous;
    private boolean fins;
    private boolean catsize;
    private boolean tail;
    private boolean domestic;

    private int legs;
    private int class_type;

    public animal(String name) {
        this.name = name;
    }

    public animal(String name, boolean hair, boolean feathers, boolean eggs,
            boolean milk, boolean airborne, boolean aquatic, boolean predator,
            boolean toothed, boolean backbone, boolean breathes, boolean venomous,
            boolean fins, int legs, boolean tail, boolean domestic,
            boolean catsize, int class_type) {
        
        this.name = name; //0
        this.hair = hair; //1
        this.feathers = feathers; //2
        this.eggs = eggs; //3
        this.milk = milk; //4
        this.airborne = airborne; //5
        this.aquatic = aquatic; //6
        this.predator = predator; //7
        this.toothed = toothed; //8
        this.backbone = backbone; //9
        this.breathes = breathes; //10
        this.venomous = venomous; //11
        this.fins = fins; //12
        this.legs = legs; //13
        this.tail = tail; //14
        this.domestic = domestic; //15
        this.catsize = catsize; //16
        this.class_type = class_type; //17
    }

    @Override
    public String toString() {
        return "animal{" + "name=" + name + ", hair=" + hair + ", feathers=" + feathers + ", eggs=" + eggs + ", milk=" + milk + ", airborne=" + airborne + ", aquatic=" + aquatic + ", predator=" + predator + ", toothed=" + toothed + ", backbone=" + backbone + ", breathes=" + breathes + ", venomous=" + venomous + ", fins=" + fins + ", catsize=" + catsize + ", tail=" + tail + ", domestic=" + domestic + ", legs=" + legs + ", class_type=" + class_type + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHair() {
        return hair;
    }

    public void setHair(boolean hair) {
        this.hair = hair;
    }

    public boolean isFeathers() {
        return feathers;
    }

    public void setFeathers(boolean feathers) {
        this.feathers = feathers;
    }

    public boolean isEggs() {
        return eggs;
    }

    public void setEggs(boolean eggs) {
        this.eggs = eggs;
    }

    public boolean isMilk() {
        return milk;
    }

    public void setMilk(boolean milk) {
        this.milk = milk;
    }

    public boolean isAirborne() {
        return airborne;
    }

    public void setAirborne(boolean airborne) {
        this.airborne = airborne;
    }

    public boolean isAquatic() {
        return aquatic;
    }

    public void setAquatic(boolean aquatic) {
        this.aquatic = aquatic;
    }

    public boolean isPredator() {
        return predator;
    }

    public void setPredator(boolean predator) {
        this.predator = predator;
    }

    public boolean isToothed() {
        return toothed;
    }

    public void setToothed(boolean toothed) {
        this.toothed = toothed;
    }

    public boolean isBackbone() {
        return backbone;
    }

    public void setBackbone(boolean backbone) {
        this.backbone = backbone;
    }

    public boolean isBreathes() {
        return breathes;
    }

    public void setBreathes(boolean breathes) {
        this.breathes = breathes;
    }

    public boolean isFins() {
        return fins;
    }

    public void setFins(boolean fins) {
        this.fins = fins;
    }

    public boolean isCatsize() {
        return catsize;
    }

    public void setCatsize(boolean catsize) {
        this.catsize = catsize;
    }

    public boolean isTail() {
        return tail;
    }

    public void setTail(boolean tail) {
        this.tail = tail;
    }

    public boolean isDomestic() {
        return domestic;
    }

    public void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }

    public int getLegs() {
        return legs;
    }

    public void setLegs(int legs) {
        this.legs = legs;
    }

    public int getClass_type() {
        return class_type;
    }

    public void setClass_type(int class_type) {
        this.class_type = class_type;
    }

    
    
}
