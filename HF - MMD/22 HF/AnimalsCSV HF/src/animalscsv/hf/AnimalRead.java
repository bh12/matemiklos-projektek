package animalscsv.hf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class AnimalRead{

    public static List<String> animalReader(String path) {

        StringBuilder sb = new StringBuilder();
        int animalsNumber = 0;
        List<String> animals = new ArrayList<>();

        try {
            FileReader fr = new FileReader(path);

            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
                animals.add(line);
                animalsNumber++;
            }

        } catch (IOException e) {
            System.out.println("Hiba történt");
        }

        return animals;
    }
    
    public static List<animal> animalLine(List<String> animals) {

        List<animal> animal = new ArrayList<>();

        for (int i = 0; i < animals.size() - 1; i++) {

            String animalLine = animals.get(0);

//            System.out.println(animals.get(i));
            String[] tmp = animals.get(i).split(";");

            animal animalTmp = new animal(tmp[0],
                    animalboolean(tmp[1]),
                    animalboolean(tmp[2]),
                    animalboolean(tmp[3]),
                    animalboolean(tmp[4]),
                    animalboolean(tmp[5]),
                    animalboolean(tmp[6]),
                    animalboolean(tmp[7]),
                    animalboolean(tmp[8]),
                    animalboolean(tmp[9]),
                    animalboolean(tmp[10]),
                    animalboolean(tmp[11]),
                    animalboolean(tmp[12]),
                    Integer.parseInt(tmp[13]),
                    animalboolean(tmp[14]),
                    animalboolean(tmp[15]),
                    animalboolean(tmp[16]),
                    Integer.parseInt(tmp[17]));

            animal.add(animalTmp);
        }

        return animal;

    }
    
    public static boolean animalboolean(String s){
        
        if(s.equalsIgnoreCase("1")){
            return true;
        }else{
            return false;
        }
        
    }
    
//    public static int animalSelect(List<animal> animals) {
//        
//        int counter = 0;
//        
//        for(int i = 0; i < animals.size(); i++){
//            
//            
//            
//        }
         
            
//    }
    
}
