package braininghub.servlets;

import braininghub.dao.CarDAO;
import braininghub.dao.CarEntity;
import java.io.IOException;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {

    @EJB
    CarDAO carDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("car.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Long carId = Long.valueOf(req.getParameter("carId"));
        String details = req.getParameter("details");

//        carDAO.addCar(carId, details);

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpaMain");
        EntityManager em = factory.createEntityManager();
        
        CarEntity ce = new CarEntity(carId, details);
        
        em.persist(ce);
        
        res.sendRedirect("MainMenuServlet");
    }
}
