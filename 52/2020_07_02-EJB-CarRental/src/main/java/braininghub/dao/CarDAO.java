package braininghub.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Singleton
public class CarDAO {
    
    @PersistenceContext
    private EntityManager em;
    
//    @Resource(name = "jdbc/bh")
//    DataSource ds;
    
//    public void addCar(int carId, String details) {
//        String insertQuery = "insert into car(car_id, details) values(?, ?)";
//        try (Connection conn = ds.getConnection();
//                PreparedStatement st = conn.prepareStatement(insertQuery);) {
//            st.setInt(1, carId);
//            st.setString(2, details);
//            
//            int rowsCreated = st.executeUpdate();
//            
//            System.out.println(rowsCreated + " cars created.");
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
}
