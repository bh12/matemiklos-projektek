package hu.mmd;

import entities.Car;
import entities.Company;
import entities.Site;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author MMD
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpaMain");
        EntityManager em = factory.createEntityManager();

//        List<Car> carList = em.createQuery("SELECT c FROM Car c").getResultList();
//        
//        for (int i = 0; i < carList.size(); i++) {
//            System.out.println(carList.get(i).getColour());
//            System.out.println(carList.get(i).getSite().getAddress());
//        }
//        List<Site> siteList = em.createQuery("SELECT s FROM Site s").getResultList();
//        
//        for (int i = 0; i < siteList.size(); i++) {
//            System.out.println(siteList.get(i).getAddress());
//            System.out.println("Hozzátartozó car színe: " + siteList.get(i).getCar().getColour());
//            
//        }
        List<Company> companyList = em.createQuery("SELECT c FROM Company c").getResultList();

        for (int i = 0; i < companyList.size(); i++) {
            for (int j = 0; j < companyList.get(i).getSite().size(); j++) {
                for (int k = 0; k < companyList.get(i).getSite().get(j).
                        getCar().size(); k++) {
                    System.out.println(companyList.get(i).getSite().get(j).
                            getCar().get(k).getColour());
                }
            }
        }
        
        
    }
}
