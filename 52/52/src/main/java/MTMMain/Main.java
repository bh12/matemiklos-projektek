package MTMMain;

import ManyToMany.Course;
import ManyToMany.Student;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author MMD
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpaMain");
        EntityManager em = factory.createEntityManager();

        System.out.println("Valami");

        List<Student> students = em.createQuery("select s from Student s").getResultList();

        for (Student s : students) {
            System.out.println(s.getName() + " a következő tárgyakat szereti:");
            for (Course c : s.getCourses()) {
                System.out.println(c.getName());
            }
        }

        Student student = new Student();
        student.setName("Mókus");
//        student.setId(-4L);
        
        EntityTransaction et = em.getTransaction();
        et.begin();
        
        em.persist(student);
        
        et.commit();
        
    }
}
