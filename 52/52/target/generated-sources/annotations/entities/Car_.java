package entities;

import entities.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-30T19:05:01")
@StaticMetamodel(Car.class)
public class Car_ { 

    public static volatile SingularAttribute<Car, String> colour;
    public static volatile SingularAttribute<Car, Site> site;
    public static volatile SingularAttribute<Car, Integer> carId;

}