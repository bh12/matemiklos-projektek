package entities;

import entities.Car;
import entities.Company;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-30T19:05:01")
@StaticMetamodel(Site.class)
public class Site_ { 

    public static volatile ListAttribute<Site, Car> cars;
    public static volatile SingularAttribute<Site, String> address;
    public static volatile SingularAttribute<Site, Company> company;
    public static volatile SingularAttribute<Site, Long> id;

}