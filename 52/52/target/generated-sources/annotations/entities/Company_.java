package entities;

import entities.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-30T19:05:01")
@StaticMetamodel(Company.class)
public class Company_ { 

    public static volatile ListAttribute<Company, Site> site;
    public static volatile SingularAttribute<Company, String> name;
    public static volatile SingularAttribute<Company, Long> id;

}