package ManyToMany;

import ManyToMany.Student;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-30T19:05:01")
@StaticMetamodel(Course.class)
public class Course_ { 

    public static volatile SingularAttribute<Course, String> name;
    public static volatile ListAttribute<Course, Student> students;
    public static volatile SingularAttribute<Course, Long> id;

}