package ManyToMany;

import ManyToMany.Course;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-30T19:05:01")
@StaticMetamodel(Student.class)
public class Student_ { 

    public static volatile ListAttribute<Student, Course> courses;
    public static volatile SingularAttribute<Student, String> name;
    public static volatile SingularAttribute<Student, Long> id;

}