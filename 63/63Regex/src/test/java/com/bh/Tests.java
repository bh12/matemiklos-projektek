package com.bh;

import java.util.regex.Pattern;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author MMD
 * 
 * 
1 Pontosan a kalap szo legyen
2 Lehet “a” vagy “b” betu pontosan egyszer
3 Barmennyi “a” vagy “b” betu lehet (nullaszor is)
4 Barmi mas lehessen benne csak “a” vagy “b” betu nem
5 Kezdodjon “bela”-val
6 Vegzodjon “alma”-val
7 Vegzodjon “a” vagy “b” betuvel
8 Tartalmazza a “hamu” szot barhol a szovegben
9 Pontosan 3 darab szam legyen
10 Legalabb 3 darab “a” betu legyen
11 Maximum 5 darab “b” betu legyen
12 Legyen 3-6 darab “c” betu
13 1000-2999 ig fogadjon el egy adott szoveget
 */
public class Tests {

    @Test
    public void test1() {
        // given
        String test = "kalap";

        // when
        boolean result = Pattern.matches("kalap", test);

        // then
        Assertions.assertTrue(result);
    }
    @Test
    
    public void test2() {
        // given
        String test = "a";

        // when
        boolean result = Pattern.matches("[ab]{1}", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test3() {
        // given
        String test = "a";

        // when
        boolean result = Pattern.matches("[ab]*", test);

        // then
        Assertions.assertTrue(result);
    }
    @Test
    
    public void test4() {
        // given
        String test = "zzz";

        // when
        boolean result = Pattern.matches("[^ab]*", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test5() {
        // given
        String test = "belaa";

        // when
        boolean result = Pattern.matches("^bela.*", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test6() {
        // given
        String test = "aalma";

        // when
        boolean result = Pattern.matches("^.*alma", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test7() {
        // given
        String test = "a";

        // when
        boolean result = Pattern.matches("[ab]$", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test8() {
        // given
        String test = "hamu";

        // when
        boolean result = Pattern.matches("^.*hamu.*$", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test9() {
        // given
        String test = "123";

        // when
        boolean result = Pattern.matches("\\d{3,3}", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test10() {
        // given
        String test = "aaa";

        // when
        boolean result = Pattern.matches("a{3,}", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test11() {
        // given
        String test = "bbbbb";

        // when
        boolean result = Pattern.matches("b{0,5}", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test12() {
        // given
        String test = "cccc";

        // when
        boolean result = Pattern.matches("c{3,6}", test);

        // then
        Assertions.assertTrue(result);
    }
    
    @Test
    public void test13() {
        // given
        String test = "1500";

        // when
        boolean result = Pattern.matches("\\b(1[0-9]{3}|2[0-8][0-9]{2}|29[0-8][0-9]|299[0-9])\\b", test);

        // then
        Assertions.assertTrue(result);
    }
}
