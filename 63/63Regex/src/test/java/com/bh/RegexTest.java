package com.bh;

import java.util.regex.Pattern;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author MMD
 */
public class RegexTest {

    @Test
    public void test() {
        // given
        String test = "12345";

        // when
        boolean result = Pattern.matches("\\d", test);

        // then
        Assertions.assertTrue(result);
    }

}
