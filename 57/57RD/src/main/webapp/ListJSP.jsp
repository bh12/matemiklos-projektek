<%-- 
    Document   : ListJSP
    Created on : 2020.08.01., 10:04:05
    Author     : MMD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello Orders and Products!</h1>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Order Date</th>
                    <th scope="col">Delivery Date</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${DAO.GetOrders()}" var="OrderDTO">
                <tr>
                    <th scope="row">${OrderDTO.id}</th>
                    <td>${OrderDTO.orderDate}</td>
                    <td>${OrderDTO.deliveryDate}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
                
</body>
</html>
