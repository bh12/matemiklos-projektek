package Mapper;

import DTO.OrderDTO;
import DTO.ProductDTO;
import Entities.OrdersEntity;
import Entities.ProductsEntity;

/**
 *
 * @author MMD
 */
public class Mapper {
    
    Mapper INSTACE;
    
    public static OrderDTO OrdersEntityToDTO(OrdersEntity ordersEntity){
        
        OrderDTO orderDTO = new OrderDTO();
        
        orderDTO.setId(ordersEntity.getId());
        orderDTO.setOrderDate(ordersEntity.getOrderDate());
        orderDTO.setDeliveryDate(ordersEntity.getDeliveryDate());
        
        return orderDTO;
    }
    
    public static ProductDTO ProductsEntityToDTO(ProductsEntity productsEntity){
        
        ProductDTO productDTO = new ProductDTO();
        
        productDTO.setId(productsEntity.getId());
        productDTO.setName(productsEntity.getName());
        productDTO.setDescription(productsEntity.getDescription());
        productDTO.setPrice(productsEntity.getPrice());
        
        return productDTO;
    }
    
}
