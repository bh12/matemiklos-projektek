package DAO;

import DTO.OrderDTO;
import DTO.ProductDTO;
import Entities.OrdersEntity;
import Entities.ProductsEntity;
import Mapper.Mapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MMD
 */

@Singleton
public class DAO {

    @PersistenceContext
    EntityManager em;

    public List<OrderDTO> GetOrders() {

        List<OrdersEntity> ordersEntityList = em.createQuery("SELECT o FROM Orders o").getResultList();

        List<OrderDTO> orderDTOList = new ArrayList<>();

        for (int i = 0; i < ordersEntityList.size(); i++) {

            orderDTOList.add(Mapper.OrdersEntityToDTO(ordersEntityList.get(i)));
        }
        
        return orderDTOList;
    }
        
    public List<ProductDTO> GetProducts(){

        List<ProductsEntity> productsEntityList = em.createQuery("SELECT p FROM Products p").getResultList();

        List<ProductDTO> productDTOList = new ArrayList<>();

        for (int i = 0; i < productsEntityList.size(); i++) {

            productDTOList.add(Mapper.ProductsEntityToDTO(productsEntityList.get(i)));
        }

        return productDTOList;
    }

}
