package Entities;

import Entities.OrdersEntity;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-08-01T13:58:31")
@StaticMetamodel(ProductsEntity.class)
public class ProductsEntity_ { 

    public static volatile SingularAttribute<ProductsEntity, Integer> price;
    public static volatile SingularAttribute<ProductsEntity, String> name;
    public static volatile SingularAttribute<ProductsEntity, String> description;
    public static volatile SingularAttribute<ProductsEntity, OrdersEntity> orders;
    public static volatile SingularAttribute<ProductsEntity, Long> id;

}