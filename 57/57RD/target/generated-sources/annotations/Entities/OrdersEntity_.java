package Entities;

import Entities.ProductsEntity;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-08-01T13:58:31")
@StaticMetamodel(OrdersEntity.class)
public class OrdersEntity_ { 

    public static volatile ListAttribute<OrdersEntity, ProductsEntity> Products;
    public static volatile SingularAttribute<OrdersEntity, Long> id;
    public static volatile SingularAttribute<OrdersEntity, String> deliveryDate;
    public static volatile SingularAttribute<OrdersEntity, String> orderDate;

}