package hu.mmd.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author MMD
 */
public class CalculatorTest {

    private static Calculator calculator;

    public CalculatorTest() {
    }

    @BeforeAll
    public static void setUpClass() {

        calculator = new Calculator();

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void add_fivePlusSix_returnsEleven() {

        long result = calculator.add(5l, 6l);

        Assertions.assertEquals(11l, result);
    }

    @Test
    public void multiply_fivemultiplyByTwo_returnsTrue() {

        long result = calculator.multiply(5l, 2l);

        assertTrue(result % 2 == 0);
    }

    @Test()
    public void divide_divisionByZero_returnsTrue() {

        ArithmeticException aeT = new ArithmeticException();
        
        try {
            long result = calculator.divide(5l, 0l);
        } catch (Exception ae) {
            assertTrue(ae.getClass() == aeT.getClass());
        }

    }
}
