/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.mmd.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author MMD
 */
public class PrimeTest {

    private static Prime prime;

    public PrimeTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {

        prime = new Prime();
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void isPrime_primeNumber5_returnsTrue() {

        boolean isPrime = prime.isPrime(5);

        assertTrue(isPrime);
    }

    @Test
    public void isPrime_primeNumber2_returnsTrue() {

        boolean isPrime = prime.isPrime(2);

        assertTrue(isPrime);
    }
    
    @Test
    public void isPrime_primeNumber44_returnsFalse() {

        boolean isPrime = prime.isPrime(44);

        assertFalse(isPrime);
    }
    
    @Test
    public void isPrime_negativeNumber_returnsFalse() {

        boolean isPrime = prime.isPrime(-5);

        assertFalse(isPrime);
    }
}
