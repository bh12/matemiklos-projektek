package hu.mmd.test;

/**
 *
 * @author MMD
 */
public class Calculator {
    
    public long add(long a, long b){
        return a + b;
    }
    
    public long multiply(long a, long b){
        return a * b;
    }
    
    public long divide(long a, long b){
        return a / b;
    }
    
}
