package hu.mmd.test;

/**
 *
 * @author MMD
 */
public class Prime {

    public boolean isPrime(int n) {

        for (int i = 2; i <= n / i; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return n > 1;
    }
}
