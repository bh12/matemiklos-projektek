package hu.mmd.rd;

import java.util.ArrayList;

/**
 *
 * @author MMD
 */
public class PrimeThread implements Runnable {

    int from;
    int to;
    public ArrayList<Integer> primeList;

    public PrimeThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        this.primeList = primes();
        
        for (int i = 0; i < primeList.size(); i++) {
            System.out.println(primeList.get(i));
        }
    }

    public ArrayList<Integer> primes() {

        ArrayList<Integer> primes = new ArrayList<>();

        for (int i = from; i < to + 1; i++) {

            int num = i;
            boolean flag = false;

            for (int j = 2; j <= num / 2; ++j) {
                if (num % i == 0) {
                    flag = true;
                    break;
                }
            }

            if (!flag) {
                primes.add(i);
            } else {
                ;
            }
        }

        return primes;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public ArrayList<Integer> getPrimeList() {
        return primeList;
    }

    public void setPrimeList(ArrayList<Integer> primeList) {
        this.primeList = primeList;
    }

}
