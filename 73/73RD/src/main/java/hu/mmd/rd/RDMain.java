package hu.mmd.rd;

import java.util.ArrayList;

/**
 *
 * @author MMD
 */
public class RDMain {

    public static void main(String[] args) {

        ArrayList<Integer> primes = new ArrayList<>();
        
        primes = NPrimeThreads.primeThreads(5, 5000, 2);
        
        for (int i = 0; i < primes.size(); i++) {
            System.out.println(primes.get(i));
        }
        
        System.out.println(primes.isEmpty());

//        Employee employee = new Employee("XY", 29, 06201234567, "Utca");
//        ArrayList<Employee> employees = new ArrayList<>();
//        employees.add(employee);
//        
//        EmployeeToFile.WriteToFile(employees);
    }
}
