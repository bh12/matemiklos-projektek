package hu.mmd.rd;

import java.io.Serializable;

/**
 *
 * @author MMD
 */
public class Employee implements Serializable{
    
    private String name;
    private int age;
    private int phoneNumber;
    private String address;

    public Employee(String name, int age, int phoneNumber, String address) {
        this.name = name;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + ", age=" + age + ", phoneNumber=" + phoneNumber + ", address=" + address + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
}
