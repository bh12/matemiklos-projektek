package hu.mmd.rd;

import java.util.ArrayList;

/**
 *
 * @author MMD
 */
public class NPrimeThreads {

    public static ArrayList<Integer> primeThreads(int from, int to, int threads) {

        ArrayList<Integer> primes = new ArrayList<>();

        int range = ((to - from) / threads);

        for (int i = 0; i < threads; i++) {

            PrimeThread pt = new PrimeThread((from + (i * range)), ((threads - i) * range));

            pt.setFrom(from + (i * range));
            pt.setTo(to + (i * from));

            System.out.println(pt.from);
            Thread t = new Thread(pt);

            t.start();

//            if (!pt.getPrimeList().isEmpty()) {
//                primes.addAll(pt.primeList);
//                t.interrupt();
//            }

        }

        return primes;

    }

}
