package hu.mmd.rd;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MMD
 */
public class EmployeeToFile {

    public static void WriteToFile(ArrayList<Employee> employees) {

//        File f = new File("emloyees.txt");
//
//        try (FileWriter fw = new FileWriter(f); BufferedWriter bw = new BufferedWriter(fw)) {
//
//            StringBuilder sb = new StringBuilder();
//
//            for (int i = 0; i < employees.size(); i++) {
//                sb.append("1. ");
//                sb.append(employees.get(i));
//                sb.append("; ");
//            }
//
//            String s = sb.toString();
//
//            bw.write(s);
//
//        } catch (IOException ex) {
//            Logger.getLogger(EmployeeToFile.class.getName()).log(Level.SEVERE, null, ex);
//        }
        try (FileOutputStream file = new FileOutputStream("emloyeesObj.ser");
                ObjectOutputStream out = new ObjectOutputStream(file)) {

            for (int i = 0; i < employees.size(); i++) {
                out.writeObject(employees.get(i));
            }

            out.close();
            file.close();

        } catch (IOException ex) {
            Logger.getLogger(EmployeeToFile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
