package main.controller;

import lombok.AllArgsConstructor;
import main.entity.CarEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
public class CarController {

    @GetMapping("/cars")
    public ResponseEntity<List<CarEntity>> getCars(){
        List<CarEntity> cars = new ArrayList<>();
        cars.add(new CarEntity("Audi", 10, 1L));
        cars.add(new CarEntity("BMW", 15, 2L));
        cars.add(new CarEntity("Porsche", 20, 3L));
        return ResponseEntity.ok(cars);
    }


}
