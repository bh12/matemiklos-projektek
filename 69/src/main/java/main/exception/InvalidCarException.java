package main.exception;

public class InvalidCarException extends RuntimeException {

    public InvalidCarException(String message) {
        super(message);
    }

}
