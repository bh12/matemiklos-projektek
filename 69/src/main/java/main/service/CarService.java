package main.service;

import lombok.AllArgsConstructor;
import main.entity.CarEntity;
import main.exception.InvalidCarException;
import main.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CarService {

    @Autowired
    private final CarRepository carRepository;

    public CarEntity save(CarEntity car){
        if("BMW".equalsIgnoreCase(car.getName())){
            throw new InvalidCarException("BMW nem lehet");
        }
    }
}
