package main.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import main.entity.CarEntity;
import org.springframework.data.repository.CrudRepository;

@Data
@AllArgsConstructor
public interface CarRepository extends CrudRepository<CarEntity, Long> {
}


